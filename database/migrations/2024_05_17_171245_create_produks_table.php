<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->foreignId('kategori_id')->constrained(table: 'kategoris', indexName: 'produks_kategori_id_foreign')->onDelete('cascade');
            $table->foreignId('user_id')->constrained(table: 'users', indexName: 'users_user_id_foreign')->onDelete('cascade');
            $table->integer('harga_list');
            $table->integer('harga_netto');
            $table->integer('stok');
            $table->string('deskripsi');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('produks');
    }
};
