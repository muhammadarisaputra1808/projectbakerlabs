<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('tokos', function (Blueprint $table) {
            $table->string('nomor_rekening')->nullable();
            $table->string('nama_pemilik')->nullable();
            $table->string('bank')->nullable();
        });
    }

    public function down()
    {
        Schema::table('tokos', function (Blueprint $table) {
            $table->dropColumn('nomor_rekening');
            $table->dropColumn('nama_pemilik');
            $table->dropColumn('bank');
        });
    }
};
