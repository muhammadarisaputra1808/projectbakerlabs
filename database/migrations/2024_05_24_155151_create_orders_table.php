<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained(table: 'users', indexName: 'orders_user_id_foreign')->onDelete('cascade');
            $table->foreignId('produk_id')->constrained(table: 'produks', indexName: 'orders_produk_id_foreign')->onDelete('cascade');
            $table->foreignId('toko_id')->constrained(table: 'tokos', indexName: 'orders_toko_id_foreign')->onDelete('cascade')->after('user_id');
            $table->integer('qty');
            $table->integer('total');
            $table->integer('sub_total');
            $table->string('no_pesanan');
            $table->date('tgl_pesanan');
            $table->string('snap_token')->nullable();
            $table->enum('status', ['Menunggu', 'Dikemas', 'Dikirim', 'Diterima'])->default('Menunggu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
