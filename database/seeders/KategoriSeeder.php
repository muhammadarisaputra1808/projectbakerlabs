<?php

namespace Database\Seeders;

use App\Models\Kategori;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data =[
            [
                'nama' => 'Kue Kering',
                'slug' => 'kue-kering',
            ],
            [
                'nama' => 'Snack',
                'slug' => 'snack',
            ],
            [
                'nama' => 'Parcel',
                'slug' => 'parcel',
            ],
            [
                'nama' => 'Kue',
                'slug' => 'kue',
            ],
            
        ];
        foreach ($data as $key => $val){
            Kategori::create($val);
        }
    }
}
