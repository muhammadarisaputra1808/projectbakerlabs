<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'kategori_id' => '1',
                'user_id' => '1',
                'nama' => 'Chocolate Jhon',
                'slug' => 'chocolate-jhon',
                'harga_list' => '109000',
                'harga_netto' => '9500',
                'stok' => '5',
                'deskripsi' => 'Lezat Dan Manis Kayak Rasa Jembut Janda',
            ],
            [
                'kategori_id' => '2',
                'user_id' => '1',
                'nama' => 'Pizza Cake',
                'slug' => 'pizza-cake',
                'harga_list' => '109000',
                'harga_netto' => '9500',
                'stok' => '5',
                'deskripsi' => 'Lezat Dan Manis Kayak Rasa Jembut Janda',
            ],
            [
                'kategori_id' => '3',
                'user_id' => '1',
                'nama' => 'Chocolate Caramel',
                'slug' => 'chocolate-caramel',
                'harga_list' => '109000',
                'harga_netto' => '9500',
                'stok' => '5',
                'deskripsi' => 'Lezat Dan Manis Kayak Rasa Jembut Janda',
            ],
            [
                'kategori_id' => '4',
                'user_id' => '1',
                'nama' => 'Maksuba Cake',
                'slug' => 'maksuba-cake',
                'harga_list' => '109000',
                'harga_netto' => '9500',
                'stok' => '5',
                'deskripsi' => 'Lezat Dan Manis Kayak Rasa Jembut Janda',
            ],
        ];
        foreach ($data as $key => $val) {
            Produk::create($val);
        }
    }
}
