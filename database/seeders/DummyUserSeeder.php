<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DummyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $userData =[
            [
                'username' => 'admin',
                'name' => 'Admin Bakerlabs',
                'email' => 'admin@gmail.com',
                'role' => 'admin',
                'password' => bcrypt('123456')
            ],
            [
                'username' => 'arisaputra',
                'name' => 'Muhammad Ari Saputra',
                'email' => 'arisaputra@gmail.com',
                'role' => 'customer',
                'password' => bcrypt('123456')
            ],
            [
                'username' => 'chaycoklat',
                'name' => 'Chaycoklat',
                'email' => 'chaycoklat@gmail.com',
                'role' => 'seller',
                'password' => bcrypt('123456')
            ],
        ];

        foreach ($userData as $key => $val){
            User::create($val);
        }
    }
}
