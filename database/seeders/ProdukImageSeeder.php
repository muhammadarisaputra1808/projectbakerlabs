<?php

namespace Database\Seeders;

use App\Models\ProdukImage;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProdukImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'produk_id' => '1',
                'images' => 'uploads/toko/bananacake.jpeg',
            ],
            [
                'produk_id' => '1',
                'images' => 'uploads/toko/bananacake.jpeg',
            ],
            [
                'produk_id' => '2',
                'images' => 'uploads/toko/chocolate.jpeg',
            ],
            [
                'produk_id' => '2',
                'images' => 'uploads/toko/chocolate.jpeg',
            ],
            [
                'produk_id' => '3',
                'images' => 'uploads/toko/nastar.jpeg',
            ],
            [
                'produk_id' => '3',
                'images' => 'uploads/toko/nastar.jpeg',
            ],
            [
                'produk_id' => '4',
                'images' => 'uploads/toko/roticoklat.jpeg',
            ],
            [
                'produk_id' => '4',
                'images' => 'uploads/toko/roticoklat.jpeg',
            ],
        ];
        foreach ($data as $key => $val) {
            ProdukImage::create($val);
        }
    }
}
