@extends('layouts.user.main')
@section('content')
<main class="main">
    <div class="page-header text-center" style="background-image: url({{ asset('img/banner-title.jpg') }})">
        <div class="container">
            <h1 class="page-title">Dashboard<span>Akun</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Akun</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="dashboard">
            <div class="container">
                <div class="row">
                    @include('layouts.user.nav-akun')
                    <div class="col-md-9 col-lg-10">
                        <div class="tab-content">
                            <div class="tab-pane " id="tab-dashboard" role="tabpanel" aria-labelledby="tab-dashboard-link">
                                {{-- <p>Hello <span class="font-weight-normal text-dark">User</span> (not <span class="font-weight-normal text-dark">User</span>? <a href="#">Log out</a>) 
                                <br>
                                From your account dashboard you can view your <a href="#tab-orders" class="tab-trigger-link link-underline">recent orders</a>, manage your <a href="#tab-address" class="tab-trigger-link">shipping and billing addresses</a>, and <a href="#tab-account" class="tab-trigger-link">edit your password and account details</a>.</p> --}}
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card border content rounded-2">
                                            <div class="card-header border text-center card-title py-3">
                                                Status Belanja
                                            </div>
                                            <div class="card-body border text-center px-0 py-5">
                                                <div class="row">
                                                    <div class="col-sm-3 my-2">
                                                        <a href="#tab-orders"  class="tab-trigger-link">
                                                            <i  style="font-size: 70px" class="fa-solid fa-clock-rotate-left"></i>
                                                            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill" style="font-size: 15px; background-color: #a66040; color: white">
                                                                @if( \App\Models\Order::where('status', 'Menunggu')->where('user_id', Auth()->user()->id)->count() )
                                                                {{ \App\Models\Order::where('status', 'Menunggu')->where('user_id', Auth()->user()->id)->groupBy('no_pesanan')->count() }}
                                                                @else
                                                                    0
                                                                @endif
                                                            </span>
                                                            <br> Menunggu                
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-3 my-2">
                                                        <a href="#tab-orders"  class="tab-trigger-link">
                                                            <i  style="font-size: 70px" class="fa-solid fa-box-open"></i>
                                                            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill" style="font-size: 15px; background-color: #a66040; color: white">
                                                                @if( $jmlDikemas )
                                                                {{ $jmlDikemas }}
                                                                @else
                                                                    0
                                                                @endif
                                                            </span>
                                                            <br> Dikemas
                                                            
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-3 my-2">
                                                        <a href="#tab-orders"  class="tab-trigger-link">
                                                            <i  style="font-size: 70px" class="fa-solid fa-box-open"></i>
                                                            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill" style="font-size: 15px; background-color: #a66040; color: white">
                                                                @if( $jmlDikirim )
                                                                {{ $jmlDikirim }}
                                                                @else
                                                                    0
                                                                @endif
                                                            </span>
                                                            <br> Dikirim
                                                        </a>
                                                    </div>
                                                    <div class="col-sm-3 my-2">
                                                        <a href="#tab-orders"  class="tab-trigger-link">
                                                            <i style="font-size: 70px" class="fa-solid fa-people-carry-box"></i>
                                                            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill" style="font-size: 15px; background-color: #a66040; color: white">
                                                                @if( $jmlDiterima )
                                                                {{ $jmlDiterima }}
                                                                @else
                                                                    0
                                                                @endif
                                                            </span> <br> Diterima
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .End .tab-pane -->

                            {{-- PESANAN SAYA --}}
                            <div class="tab-pane fade" id="tab-orders" role="tabpanel" aria-labelledby="tab-orders-link">
                                <div class="page-content">
                                    <div class="table-responsive">
                                        <table class="table-order table table-sm mb-2 m-0 p-0">
                                            <thead>
                                                <h4 class="page-title" style="text-align: center;">Riwayat Pesanan</h4>
                                                <tr>
                                                    <th>Tanggal Pesanan</th>
                                                    <th>No Pesanan</th>
                                                    <th>Total</th>
                                                    <th>Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($pesanan->count() != 0)
                                                    @foreach ($pesanan as $item)    
                                                    <tr>
                                                        <td>{{ $item->tgl_pesanan }}</td>
                                                        <td>{{ $item->no_pesanan }}</td>
                                                        <td>Rp. {{ number_format($item->sub_total) }}</td>
                                                        <td>
                                                            @if ($item->status == 'Sudah Bayar')
                                                                <span class="in-stock">{{ $item->status }}</span>
                                                            @else
                                                                <span class="d-block text-center alert @if($item->status == 'Menunggu') alert-secondary @elseif($item->status == 'Dikemas') alert-warning @elseif($item->status == 'Dikirim') alert-info @elseif($item->status == 'Diterima') alert-success @endif">{{ $item->status }}</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('pesanan.show.details', $item->no_pesanan) }}" class="btn btn-primary">Lihat Pesanan</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="6" class="text-center">Belum Ada Pesanan</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table><!-- End .table table-wishlist -->
                                    </div>
                                </div><!-- End .page-content -->
                            </div><!-- .End .tab-pane -->

                            <div class="tab-pane fade show active" id="tab-account" role="tabpanel" aria-labelledby="tab-account-link">
                                <div class="product-details-tab">
                                    <ul class="nav nav-pills justify-content-center" role="tablist">
                                        {{-- <li class="nav-item">
                                            <a class="nav-link" id="profile" data-toggle="tab" href="#profile-tab" role="tab" aria-controls="profile-tab" aria-selected="true">Profil</a>
                                        </li> --}}
                                        <li class="nav-item">
                                            <a class="nav-link active" id="alamat" data-toggle="tab" href="#alamat-tab" role="tab" aria-controls="alamat-tab" aria-selected="false">Alamat</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="profile-tab" role="tabpanel" aria-labelledby="profile">
                                            <div class="product-desc-content">
                                                <h3>Profile Akun</h3>
                                                <div class="row">
                                                    <div class="col-sm-5 col-xl-4 p-3">
                                                        @if ($user->profile)    
                                                        <figure class="store-media mb-1 mb-lg-0 bg-light border d-flex justify-content-center align-items-center">
                                                            <img id="preview" src="{{ asset($user->profile->image ?? 'img/default.jpg') }}" alt="image" style="max-height: 200px">
                                                        </figure><!-- End .store-media -->
                                                        <form action="{{ route('profile.photo.update', $user->profile->id) }}" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('PUT')
                                                            <div>
                                                                <div class="form-group mt-1">
                                                                    <div class="row">
                                                                        <div class="col-auto mb-1">
                                                                            <input type="file" name="image" id="file" class="d-none" onchange="previewImage(event)">
                                                                            <label for="file" class="btn btn-primary m-0">Ganti Photo<i class="fa-regular fa-image"></i></label>
                                                                            
                                                                        </div>
                                                                        <div class="col-auto">
                                                                            <button type="submit" class="btn btn-primary">Perbarui<i class="fa-regular fa-pen-to-square"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    @error('image')
                                                                    <span style="color: red" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                                
                                                            </div>
                                                        </form>
                                                        @else    
                                                        <figure class="store-media mb-1 mb-lg-0 bg-light border d-flex justify-content-center align-items-center">
                                                            <img id="preview" src="{{ asset('img/default.jpg') }}" alt="image" style="max-height: 200px">
                                                        </figure><!-- End .store-media -->
                                                        @endif
                                                        
                                                        
                                                        </div><!-- End .col-xl-6 -->
                                                        <div class="col-sm-7 col-xl-8">
                                                            <div class="product-desc-content">
                                                                <div class="form-group mb-1">
                                                                    <label for="name">Nama Lengkap </label>
            
                                                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" autocomplete="name" placeholder="Masukkan Nama Lengkap ...">
                                                                    @error('name')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="product-desc-content">
                                                                <div class="form-group mb-1">
                                                                    <label for="email">Email *</label>
            
                                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" autocomplete="email" placeholder="Masukkan Nama Lengkap ...">
                                                                    @error('email')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="product-desc-content">
                                                                <div class="form-group mb-1">
                                                                    <label for="no_telepon">No Whatsapp / Telepon *</label>
            
                                                                    <input id="no_telepon" type="text" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" value="{{ $user->profile->no_telepon ?? ''}}" autocomplete="no_telepon" placeholder="Masukkan Nama Lengkap ...">
                                                                    @error('no_telepon')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- End .col-xl-6 -->
                                                </div>
                                            </div><!-- End .product-desc-content -->
                                        </div><!-- .End .tab-pane -->
                                        <div class="tab-pane fade active show" id="alamat-tab" role="tabpanel" aria-labelledby="alamat">
                                            <form action="{{ route('profile.alamat.save') }}" method="post">
                                                @csrf
                                                <div class="product-desc-content mt-2">
                                                    <h3>Tambah Alamat</h3>
                                                    <div class="form-group mb-1">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="nama">Nama Lengkap *</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" autocomplete="nama" placeholder="Masukkan Nama Lengkap ...">
                                                                @error('nama')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="no_telepon">No Whatsapp / Telepon *</label>
                                                                <input id="no_telepon" type="text" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" value="{{ old('no_telepon') }}" autocomplete="no_telepon" placeholder="Masukkan Nomor Telepon ...">
                                                                @error('no_telepon')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="kecamatan">Kecamatan *</label>
                                                                <select name="kecamatan" id="kecamatan" class="form-control @error('kecamatan') is-invalid @enderror" value="{{ old('kecamatan') }}">
                                                                    <option value="">-- Pilih Kecamatan --</option>
                                                                    @foreach ($kecamatan as $item)
                                                                    <option value="{{ $item }}" {{ old('kecamatan') == $item ? 'selected' : ''}}>{{ $item }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @error('kecamatan')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="alamat">Alamat Lengkap *</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') }}" autocomplete="alamat" placeholder="Masukkan Alamat Lengkap ...">
                                                                @error('alamat')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label for="kode_pos">Kode Pos *</label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        <input id="kode_pos" type="text" class="form-control @error('kode_pos') is-invalid @enderror" name="kode_pos" value="{{ old('kode_pos') }}" autocomplete="kode_pos" placeholder="Masukkan Kode Pos ...">
                                                                        @error('kode_pos')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-rounded">Simpan</button>
                                                </div><!-- End .product-desc-content -->
                                            </form>
                                        </div><!-- .End .tab-pane -->
                                    </div><!-- End .tab-content -->
                                </div>
                            </div><!-- .End .tab-pane -->
                        </div>
                    </div><!-- End .col-lg-9 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .dashboard -->
    </div><!-- End .page-content -->
</main><!-- End .main -->    
@endsection