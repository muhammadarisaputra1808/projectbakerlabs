@extends('layouts.user.main')
@section('content')
<main class="main">    
    <div class="intro-section">
        {{-- PELAYANAN --}}
        <div class="icon-boxes-container pt-0 pb-0">
            <div class="container">
                <div class="owl-carousel owl-simple" data-toggle="owl" 
                    data-owl-options='{
                        "nav": false, 
                        "dots": false,
                        "margin": 30,
                        "loop": false,
                        "autoplay": true,
                        "autoplayTimeout": 8000,
                        "responsive": {
                            "0": {
                                "items":1
                            },
                            "480": {
                                "items":2
                            },
                            "992": {
                                "items":3
                            },
                            "1200": {
                                "items":4
                            }
                        }
                    }'>
                        <div class="icon-box icon-box-side">
                            <span class="icon-box-icon">
                                <i class="icon-rocket"></i>
                            </span>

                            <div class="icon-box-content">
                                <h3 class="icon-box-title">PENGIRIMAN MURAH</h3><!-- End .icon-box-title -->
                                <p>SEKOTA PALEMBANG</p>
                            </div><!-- End .icon-box-content -->
                        </div><!-- End .icon-box -->
                    
                        <div class="icon-box icon-box-side">
                            <span class="icon-box-icon">
                                <i class="icon-rotate-left"></i>
                            </span>

                            <div class="icon-box-content">
                                <h3 class="icon-box-title">GARANSI SAMPAI TUJUAN</h3><!-- End .icon-box-title -->
                                <p>JIKA RUSAK DALAM PERJALANAN</p>
                            </div><!-- End .icon-box-content -->
                        </div><!-- End .icon-box -->

                        <div class="icon-box icon-box-side">
                            <span class="icon-box-icon">
                                <i class="icon-info-circle"></i>
                            </span>

                            <div class="icon-box-content">
                                <h3 class="icon-box-title">DISKON PEMBELIAN</h3><!-- End .icon-box-title -->
                                <p>Di Setiap Pembelian</p>
                            </div><!-- End .icon-box-content -->
                        </div><!-- End .icon-box -->

                        <div class="icon-box icon-box-side">
                            <span class="icon-box-icon">
                                <i class="icon-life-ring"></i>
                            </span>

                            <div class="icon-box-content">
                                <h3 class="icon-box-title">KAMI MENDUKUNG</h3><!-- End .icon-box-title -->
                                <p>24/7 Pelayanan Terbaik</p>
                            </div><!-- End .icon-box-content -->
                        </div><!-- End .icon-box -->
                </div><!-- End .owl-carousel -->
            </div><!-- End .container -->
        </div><!-- End .icon-boxes-container -->

        {{-- BANNER --}}
        <div class="intro-section-slider">
            <div class="container">
                <div class="intro-slider-container slider-container-ratio mb-0">
                    <div class="intro-slider owl-carousel owl-simple owl-light" data-toggle="owl" 
                        data-owl-options='{
                            "nav": false, 
                            "dots": true,
                            "responsive": {
                                "1200": {
                                    "nav": true,
                                    "dots": false
                                }
                            }
                        }'>
                        <div class="intro-slide">
                            <figure class="slide-image">
                                <picture>
                                    <source media="(max-width: 480px)" srcset="{{asset('img/slide-1-480w.jpg')}}">
                                    <img src="{{asset('img/slide-1.jpg')}}" alt="Image Desc">
                                </picture>
                            </figure><!-- End .slide-image -->
                        </div><!-- End .intro-slide -->

                        <div class="intro-slide">
                            <figure class="slide-image">
                                <picture>
                                    <source media="(max-width: 480px)" srcset="{{asset('img/slide-2-480w.jpg')}}">
                                    <img src="{{asset('img/slide-2.jpg')}}" alt="Image Desc">
                                </picture>
                            </figure><!-- End .slide-image -->
                        </div><!-- End .intro-slide -->

                        <div class="intro-slide">
                            <figure class="slide-image">
                                <picture>
                                    <source media="(max-width: 480px)" srcset="{{asset('img/slide-3-480w.jpg')}}">
                                    <img src="{{asset('img/slide-3.jpg')}}" alt="Image Desc">
                                </picture>
                            </figure><!-- End .slide-image -->
                        </div><!-- End .intro-slide -->
                    </div><!-- End .intro-slider owl-carousel owl-simple -->
                            
                    <span class="slider-loader"></span><!-- End .slider-loader -->
                </div><!-- End .intro-slider-container -->
            </div><!-- End .container -->
        </div><!-- End .intro-section-slider -->
    </div><!-- End .intro-section -->

    {{-- KATEGORI --}}
    <div class=" pt-3 pb-3">
        <div class="container">
            <div class="banner-group">
                <div class="row">
                    <div class="col-sm-6 col-lg-4">
                        <div class="shadow-sm bg-body rounded banner banner-overlay banner-lg">
                            <a href="{{ url('/menu/'.$kategori[0]->slug) }}">
                                <img src="{{asset('img/KATEGORI-COOKIES.jpg')}}" alt="Banner">
                            <div class="banner-content banner-content-bottom">
                                <h3 class="banner-title text-white"><a href="{{ url('/menu/'.$kategori[0]->slug) }}">{{ $kategori[0]->nama }}</a></h3><!-- End .banner-title -->
                                <a href="{{ url('/menu/'.$kategori[0]->slug) }}" class="btn btn-outline-white banner-link">Beli Sekarang</a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->
                    </div><!-- End .col-lg-4 -->
                    <div class="col-sm-6 col-lg-4 order-lg-last">
                        <div class="shadow-sm bg-body rounded banner banner-overlay banner-lg">
                            <a href="{{ url('/menu/'.$kategori[3]->slug) }}">
                                <img src="{{asset('img/KATEGORI-CAKE.jpg')}}" alt="Banner">
                            </a>
                            <div class="banner-content banner-content-top">
                                <h3 class="banner-title text-white"><a href="{{ url('/menu/'.$kategori[3]->slug) }}">{{ $kategori[3]->nama }}</a></h3><!-- End .banner-title -->
                                <a href="{{ url('/menu/'.$kategori[3]->slug) }}" class="btn btn-outline-white banner-link">Beli Sekarang</a>
                            </div><!-- End .banner-content -->
                        </div><!-- End .banner -->
                    </div><!-- End .col-lg-4 -->
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-sm-6 col-lg-12">
                                <div class="shadow-sm bg-body rounded banner banner-overlay">
                                    <a href="{{ url('/menu/'.$kategori[1]->slug) }}">
                                        <img src="{{asset('img/KATEGORI-SNACK.jpg')}}" alt="Banner">
                                    </a>
                                    <div class="banner-content">
                                        <h3 class="banner-title text-white"><a href="{{ url('/menu/'.$kategori[1]->slug) }}">{{ $kategori[1]->nama }}</a></h3><!-- End .banner-title -->
                                        <a href="{{ url('/menu/'.$kategori[1]->slug) }}" class="btn btn-outline-white banner-link">Beli Sekarang</a>
                                    </div><!-- End .banner-content -->
                                </div><!-- End .banner -->
                            </div><!-- End .col-sm-6 col-lg-12 -->
                            <div class="col-sm-6 col-lg-12">
                                <div class="shadow-sm bg-body rounded banner banner-overlay">
                                    <a href="{{ url('/menu/'.$kategori[2]->slug) }}">
                                        <img src="{{asset('img/KATEGORI-PARCEL.jpg')}}" alt="Banner">
                                    </a>
                                    <div class="banner-content">
                                        <h3 class="banner-title text-white"><a href="{{ url('/menu/'.$kategori[2]->slug) }}">{{ $kategori[2]->nama }}</a></h3><!-- End .banner-title -->
                                        <a href="{{ url('/menu/'.$kategori[2]->slug) }}" class="btn btn-outline-white banner-link">Beli Sekarang</a>
                                    </div><!-- End .banner-content -->
                                </div><!-- End .banner -->
                            </div><!-- End .col-sm-6 col-lg-12 -->
                        </div><!-- End .row -->
                    </div><!-- End .col-lg-4 -->
                </div><!-- End .row -->
            </div><!-- End .banner-group -->
        </div><!-- End .container -->
    </div>

    <div class="page-header text-center" style="background-image: url('{{asset('img/banner-title.jpg')}}')">
        <div class="container">
            <h1 class="page-title">PRODUCT<span>recomended</span></h1>
        </div><!-- End .container -->
    </div>

    <div class="bg-light-2 container-fluid pt-3">
        <div class="heading heading-center mb-3">
            <ul class="nav nav-pills nav-border-anim justify-content-center" role="tablist">
                <li class="nav-item">
                    <a href="{{ route('produks.show') }}" class="nav-link active">Lihat Produk Lainnya ></a><!-- End .title -->
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane p-0 fade active show">
                <div class="products">
                    <div class="row justify-content-center">
                        @foreach ($produks as $produk)
                        <div class="col-6 col-md-4 col-lg-3 col-xl-5col">
                            <div class="product product-7 text-center shadow-sm bg-body rounded">
                                <figure class="square-figure" style="position: relative; width: 100%;">
                                    <div class="square-container" style="padding-top: 100%; position: relative;">
                                        <a href="{{ url('menu/'.$produk->slug.'/details') }}">
                                            @if ($produk->images->isEmpty())
                                                <img src="{{ asset('img/default.jpg') }}" alt="Product image" class="product-image">    
                                            @else
                                                {{-- <img src="{{ asset($produk->images->first()->images) }}" alt="Product image" class="product-image">     --}}
                                                <img src="{{ asset($produk->images->first()->images) }}" alt="Product image" class="product-image" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; object-fit: cover;">
                                            @endif
                                        </a>
                                    </div>  
                                    <div class="product-action">
                                        <a href="#" class="btn-product btn-cart" onclick="event.preventDefault(); document.getElementById('add-to-cart-form-{{ $produk->id }}').submit();">
                                            <span>Tambah Ke Keranjang</span>
                                        </a>
                                    </div><!-- End .product-action -->
                                    <form id="add-to-cart-form-{{ $produk->id }}" action="{{ route('add-to-cart') }}" method="POST" style="display: none;">
                                        @csrf
                                        <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                                        <input type="hidden" name="qty" value="1">
                                    </form>
                                </figure><!-- End .product-media -->                                
        
                                <div class="product-body">
                                    <div class="product-cat">
                                        <a>{{ $produk->kategori->nama }}</a>
                                    </div><!-- End .product-cat -->
                                    <h3 class="product-title"><a href="{{ url('/menu/'.$produk->slug) }}">{{ $produk->nama }}</a></h3><!-- End .product-title -->
                                    <div class="product-price">
                                        Rp. {{ number_format($produk->harga_netto, 0, ',', '.') }}
                                    </div><!-- End .product-price -->
                                    <div class="product-content">
                                        {{ $produk->accepted_orders_count }} Terjual
                                    </div>
                                    @if ($produk->reviews->isNotEmpty())
                                        @foreach($produk->reviews as $review)
                                            <div class="ratings-container">
                                                <div class="ratings">
                                                    <div class="ratings-val" style="width: {{ ($review->rating / 5) * 100 }}%;"></div><!-- End .ratings-val -->
                                                </div><!-- End .ratings -->
                                                <span class="ratings-text">({{ $review->rating }} / 5)</span>
                                            </div><!-- End .ratings-container -->
                                        @endforeach
                                    @else     
                                    <div class="ratings-container">
                                        <div class="ratings">
                                            <div class="ratings-val" style="width: 0%;"></div><!-- End .ratings-val -->
                                        </div><!-- End .ratings -->
                                        <span class="ratings-text">( 0 / 5 )</span>
                                    </div><!-- End .rating-container -->
                                    @endif
                                </div><!-- End .product-body -->
                            </div><!-- End .product -->
                        </div><!-- End .col-sm-6 col-lg-4 col-xl-3 -->
                        @endforeach
                    </div><!-- End .row -->
                </div>
            </div>
        </div>
    </div>
</main><!-- End .main -->
@endsection           