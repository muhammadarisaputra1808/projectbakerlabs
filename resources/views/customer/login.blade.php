@extends('layouts.user.main')
@section('content')
<main class="main"> 
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="form-box mt-3 mb-3">
                    <div class="form-tab">
                        <ul class="nav nav-pills nav-fill nav-border-anim" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="signin" aria-selected="true">Masuk</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="tab-content-5">
                            <div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="signin-tab">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $item)
                                                <li>{{$item}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                    </div><!-- End .form-group -->

                                    <div class="form-group">
                                        <label>Password *</label>
                                        <input type="password" name="password" class="form-control">
                                    </div><!-- End .form-group -->

                                    <div class="form-footer" style="display: flex; flex-direction: column; align-items: center;">
                                        <button type="submit" class="btn btn-outline-primary-2" name="submit" style="margin: 5px 0;">
                                            <span>Masuk</span>
                                            <i class="icon-long-arrow-right"></i>
                                        </button>
                                        <a href="#" class="forgot-link" style="margin: 5px 0;">Forgot Your Password?</a>
                                    </div><!-- End .form-footer -->
                                </form>
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .form-tab -->
                </div><!-- End .form-box -->
            </div>
        </div>
    </div>
    
</main><!-- End .main -->
@endsection