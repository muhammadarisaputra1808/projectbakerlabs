@extends('layouts.user.main')
@section('content')
<main class="main">
    <div class="page-header text-center" style="background-image: url({{ asset('img/banner-title.jpg') }})">
        <div class="container">
            <h1 class="page-title">Produk<span>Shop</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Produk</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->
    @if(session('swal_success'))
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: '{{ session('swal_success') }}',
                });
            });
        </script>
    @endif
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="toolbox">
                        {{-- <div class="toolbox-left">
                            <div class="toolbox-info">
                                Menampilkan <span>{{ $produks->perPage() }} dari {{ $produks->total() }}</span> Produk
                            </div><!-- End .toolbox-info -->
                        </div> --}}
                        <div class="toolbox-right">
                            <div class="toolbox-sort">
                                <label for="sort_by">Urutkan :</label>
                                <form method="GET" action="{{ route('produks.search') }}" id="searchForm">
                                    <div class="select-custom">
                                        <select name="sort_by" id="sort_by" class="form-control">
                                            <option value="" disabled selected>Pilih Urutan</option>
                                            <option value="harga_terendah" {{ request()->sort_by == 'harga_terendah' ? 'selected' : '' }}>Harga Terendah</option>
                                            <option value="harga_tertinggi" {{ request()->sort_by == 'harga_tertinggi' ? 'selected' : '' }}>Harga Tertinggi</option>
                                            <option value="terbaru" {{ request()->sort_by == 'terbaru' ? 'selected' : '' }}>Terbaru</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary" hidden>Search</button>
                                </form>
                            </div><!-- End .toolbox-sort -->
                        </div><!-- End .toolbox-right -->
                    </div><!-- End .toolbox -->

                    <div class="products mb-3">
                        @foreach ($produks as $produk)
                        <div class="product product-list">
                            <div class="row">
                                <div class="col-6 col-lg-3">
                                    <figure class="product-media">
                                            <a href="{{ url('menu/'.$produk->slug.'/details') }}">
                                                @if ($produk->images->isEmpty())
                                                    <img src="{{ asset('img/default.jpg') }}" alt="Product image" class="product-image">    
                                                @else
                                                    <img src="{{ asset($produk->images->first()->images) }}" alt="Product image" class="product-image">    
                                                @endif
                                            </a>
                                    </figure><!-- End .product-media -->
                                </div><!-- End .col-sm-6 col-lg-3 -->

                                <div class="col-6 col-lg-4 order-lg-last">
                                    <div class="product-list-action">
                                        <div class="product-price m-0" >
                                            Rp. {{ number_format($produk->harga_netto, 0, ',', '.') }}
                                        </div><!-- End .product-price -->
                                        <div class="product-content mb-1">
                                            {{ $produk->accepted_orders_count }} Terjual
                                        </div>
                                        @if ($produk->reviews->isNotEmpty())
                                        @foreach($produk->reviews as $review)
                                            <div class="ratings-container">
                                                <div class="ratings">
                                                    <div class="ratings-val" style="width: {{ ($review->rating / 5) * 100 }}%;"></div><!-- End .ratings-val -->
                                                </div><!-- End .ratings -->
                                                <span class="ratings-text">({{ $review->rating }} / 5)</span>
                                            </div><!-- End .ratings-container -->
                                        @endforeach
                                        @else     
                                        <div class="ratings-container">
                                            <div class="ratings">
                                                <div class="ratings-val" style="width: 0%;"></div><!-- End .ratings-val -->
                                            </div><!-- End .ratings -->
                                            <span class="ratings-text">( 0 / 5 )</span>
                                        </div><!-- End .rating-container -->
                                        @endif
                                        <div>
                                            <a href="#" class="btn btn-primary btn-rounded mb-2" onclick="event.preventDefault(); document.getElementById('add-to-cart-form-{{ $produk->id }}').submit();">
                                                <i class="fa-solid fa-cart-plus"></i>
                                                <span>Tambah Ke Keranjang</span>
                                            </a>
                                        </div>
                                        <form id="add-to-cart-form-{{ $produk->id }}" action="{{ route('add-to-cart') }}" method="POST" style="display: none;">
                                            @csrf
                                            <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                                            <input type="hidden" name="qty" value="1">
                                        </form>
                                    </div><!-- End .product-list-action -->
                                </div><!-- End .col-sm-6 col-lg-3 -->

                                <div class="col-lg-5">
                                    <div class="product-body product-action-inner">
                                        <div class="product-cat">
                                            <a>{{ $produk->kategori->nama }}</a>
                                        </div><!-- End .product-cat -->
                                        <h3 class="product-title">
                                            <a href="{{ url('menu/'.$produk->slug.'/details') }}">{{ $produk->nama }}</a>
                                        </h3>

                                        <div class="product-content">
                                            <p>{{ $produk->deskripsi}} </p>
                                        </div><!-- End .product-content -->

                                        <div class="product-nav product-nav-thumbs">
                                            @foreach ($produk->images as $item)    
                                                    <a href="#" class="active">
                                                        <img src="{{ asset($item->images) }}"alt="product desc">
                                                    </a>
                                            @endforeach
                                        </div><!-- End .product-nav -->
                                    </div><!-- End .product-body -->
                                </div><!-- End .col-lg-6 -->
                            </div><!-- End .row -->
                        </div><!-- End .product -->
                        @endforeach
                    </div><!-- End .products -->
                    <div class="d-flex justify-content-center">
                        {{ $produks->links('vendor.pagination.custom-pagination') }}
                    </div>
                </div><!-- End .col-lg-9 -->
                <aside class="col-lg-3 order-lg-first">
                    <div class="sidebar sidebar-shop">
                        <div class="widget widget-collapsible">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-1" role="button" aria-expanded="true"
                                    aria-controls="widget-1">
                                    Category
                                </a>
                            </h3><!-- End .widget-title -->

                            <div class="collapse show" id="widget-1">
                                <div class="widget-body">
                                    <div class="filter-items">
                                        <div class="filter-item">
                                            @foreach ($kategori as $item)
                                            <div class="mx-3">
                                                <a href="{{ url('/menu/'.$item->slug ) }}">{{ $item->nama }}</a>
                                            </div><!-- End .custom-checkbox -->
                                            @endforeach
                                        </div><!-- End .filter-item -->
                                    </div><!-- End .filter-items -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                        <div class="widget widget-collapsible">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-5" role="button" aria-expanded="true"
                                    aria-controls="widget-5">
                                    Price
                                </a>
                            </h3><!-- End .widget-title -->

                            <div class="collapse show" id="widget-5">
                                <div class="widget-body">
                                    <div class="filter-price">
                                        <form method="GET" action="{{ route('produks.search') }}">
                                            <div class="form-group">
                                                <label for="min_price">Min Harga</label>
                                                <input type="number" class="form-control" id="min_price" name="min_price" value="{{ request()->min_price }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="max_price">Max Harga</label>
                                                <input type="number" class="form-control" id="max_price" name="max_price" value="{{ request()->max_price }}">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Cari</button>
                                        </form>
                                    </div><!-- End .filter-price -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                    </div><!-- End .sidebar sidebar-shop -->
                </aside><!-- End .col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        const orderBy = document.getElementById('sort_by');
        orderBy.addEventListener('change', () => {
            const form = document.getElementById('searchForm');
            form.submit();
        });
    });
</script>
@endsection