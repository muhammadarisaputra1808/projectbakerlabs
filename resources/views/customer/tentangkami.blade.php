@extends('layouts.user.main')
@section('content')
<main class="main">    
    <div class="page-header text-center mb-3" style="background-image: url('{{asset('img/banner-title.jpg')}}')">
        <div class="container">
            <h1 class="page-title">Tentang Kami<span>Who We Are</span></h1>
        </div><!-- End .container -->
    </div>
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tentang Kami</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content pb-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-3 mb-lg-0">
                    <h2 class="title">Visi Kami</h2><!-- End .title -->
                    <p>"Menjadi wadah inovasi bagi industri bakery di Palembang,
                        mendorong adopsi teknologi dalam pengalaman belanja kue dan produk bakery,
                        serta menjadi sumber inspirasi bagi pengusaha bakery lokal untuk berkembang dan bersaing secara global."</p>
                </div><!-- End .col-lg-6 -->
                
                <div class="col-lg-6">
                    <h2 class="title">Misi Kami</h2><!-- End .title -->
                    <p>"Memberikan pengalaman belanja kue terbaik di Palembang,
                        mendukung industri bakery lokal, memberdayakan pengusaha lokal,
                        berkontribusi pada kesejahteraan masyarakat, mengutamakan praktik bisnis yang ramah lingkungan,
                        serta mendorong inovasi dan kolaborasi dalam industri bakery."</p>
                </div><!-- End .col-lg-6 -->
            </div><!-- End .row -->

            <div class="mb-5"></div><!-- End .mb-4 -->
        </div><!-- End .container -->

        <div class="bg-light-2 pt-6 pb-5 mb-6 mb-lg-8">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mb-3 mb-lg-0">
                        <h2 class="title">Apa itu BakerLabs ?</h2><!-- End .title -->
                        <p class="lead text-primary mb-3">Bakerlabs adalah sebuah platform e-commerce
                            <br>yang berfokus pada industri bakery dan berbasis di Palembang.</p><!-- End .lead text-primary -->
                        <p class="mb-2">Bakerlabs menyediakan akses mudah untuk menemukan, membeli, dan menikmati berbagai kue berkualitas tinggi secara online. Dengan dukungan produsen lokal, kami hadirkan produk dengan cita rasa autentik Palembang. Selain sebagai tempat berbelanja, kami juga menjadi laboratorium inovasi, mendukung pertumbuhan industri bakery lokal, dan aktif dalam program sosial serta lingkungan.</p>
                    </div><!-- End .col-lg-5 -->

                    <div class="col-lg-6 offset-lg-1">
                        <div class="about-images">
                            <img src="{{asset('img/tentang-kami-1.jpg')}}" alt="" class="about-img-front" width="70%">
                            <img src="{{asset('img/tentang-kami-2.jpg')}}" alt="" class="about-img-back" width="45%">
                        </div><!-- End .about-images -->
                    </div><!-- End .col-lg-6 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .bg-light-2 pt-6 pb-6 -->

        <div class="container">
            <h2 class="title text-center mb-4">Temui Tim Kami</h2><!-- End .title text-center mb-2 -->

            <div class="row">
                <div class="col-md-4">
                    <div class="member member-anim text-center">
                        <figure class="member-media">
                            <img src="{{asset('img/member.jpg')}}" alt="member photo">
                            <figcaption class="member-overlay">
                                <div class="member-overlay-content">
                                    <h3 class="member-title">Muhammad Ari Saputra<span>Founder & CTO</span></h3><!-- End .member-title -->
                                    <div class="social-icons social-icons-simple">
                                        <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                    </div><!-- End .soial-icons -->
                                </div><!-- End .member-overlay-content -->
                            </figcaption><!-- End .member-overlay -->
                        </figure><!-- End .member-media -->
                        <div class="member-content">
                            <h3 class="member-title">Muhammad Ari Saputra<span>Founder & CTO</span></h3><!-- End .member-title -->
                        </div><!-- End .member-content -->
                    </div><!-- End .member -->
                </div><!-- End .col-md-4 -->

                <div class="col-md-4">
                    <div class="member member-anim text-center">
                        <figure class="member-media">
                            <img src="{{asset('img/member.jpg')}}" alt="member photo">

                            <figcaption class="member-overlay">
                                <div class="member-overlay-content">
                                    <h3 class="member-title">Muhammad Okta Riansyah<span>Founder & CMO</span></h3><!-- End .member-title -->
                                    <div class="social-icons social-icons-simple">
                                        <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                    </div><!-- End .soial-icons -->
                                </div><!-- End .member-overlay-content -->
                            </figcaption><!-- End .member-overlay -->
                        </figure><!-- End .member-media -->
                        <div class="member-content">
                            <h3 class="member-title">Muhammad Okta Riansyah<span>Founder & CMO</span></h3><!-- End .member-title -->
                        </div><!-- End .member-content -->
                    </div><!-- End .member -->
                </div><!-- End .col-md-4 -->

                <div class="col-md-4">
                    <div class="member member-anim text-center">
                        <figure class="member-media">
                            <img src="{{asset('img/member.jpg')}}" alt="member photo">

                            <figcaption class="member-overlay">
                                <div class="member-overlay-content">
                                    <h3 class="member-title">Brian Prasetyo Arif Wijaya<span>Founder & CMO</span></h3><!-- End .member-title -->
                                    <div class="social-icons social-icons-simple">
                                        <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                    </div><!-- End .soial-icons -->
                                </div><!-- End .member-overlay-content -->
                            </figcaption><!-- End .member-overlay -->
                        </figure><!-- End .member-media -->
                        <div class="member-content">
                            <h3 class="member-title">Brian Prasetyo Arif Wijaya<span>Founder & CMO</span></h3><!-- End .member-title -->
                        </div><!-- End .member-content -->
                    </div><!-- End .member -->
                </div><!-- End .col-md-4 -->
            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb-2"></div><!-- End .mb-2 -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
@endsection