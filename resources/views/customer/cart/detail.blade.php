@extends('layouts.user.main')
@section('content')
<main class="main">
    <div class="page-header text-center" style="background-image: url('{{ asset('img/banner-title.jpg') }}')">
        <div class="container">
            <h1 class="page-title">Checkout<span>Shop</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/cart') }}">Keranjang</a></li>
                <li class="breadcrumb-item active" aria-current="page">Checkout</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->
    <div class="page-content">
        <div class="cart">
            <div class="container">
                <div class="row">
                    <!-- Summary section -->
                    <aside class="col-lg-12">
                        <div class="summary">
                            <h3 class="summary-title" style="text-align: center;"><b>PESANAN</b></h3>
                            <table class="table table-sm mb-2 m-0 p-0">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;width: 60%"><b>Produk</b></th>
                                        <th style="text-align: center;"><b>Total</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ordersByStore as $storeName => $orders)
                                    <tr class="summary-shipping-estimate">
                                        <td class="product-col">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <h3 class="price-col">
                                                        <p class="font-weight-bold mb-1">
                                                            Toko : {{ $storeName }} <br>
                                                        </p>
                                                        @foreach ($orders as $order)
                                                            <b>{{ $order->produk->nama }}</b> <br>
                                                            
                                                            Rp. {{ number_format($order->produk->harga_netto, 0, ',', '.')
                                                            }} X {{ $order->qty }}
                                                            <br><br>
                                                        @endforeach
                                                    </h3>
                                                </div>
                                                <div class="col-auto">
                                                    <b>Biaya Pengiriman:</b> <br>
                                                    {{ $biayaPengiriman }}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="total-col" style="text-align: end;">
                                            Rp. {{ number_format($orders->first()->sub_total + $biayaPengiriman, 0, ',', '.') }}
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tr>
                                    <tr>
                                        <td><b>Biaya Layanan:</b></td>
                                        <td style=" text-align: end;">{{ $biayaLayanan }}</td>
                                    </tr>
                                    <tr class="summary-total">
                                        <td><b>Total Belanja:</b></td>
                                        <td style=" text-align: end;">Rp. {{ number_format($totalBelanja, 0, ',', '.') }}</td>
                                    </tr><!-- End .summary-total -->
                                </tbody>
                            </table><!-- End .table table-summary -->
                            <h3 class="summary-title" style="text-align: center;"><b>PENGIRIMAN</b></h3>
                            <table class="table table-summary mb-2">
                                <tbody>
                                    <tr class="summary-shipping-estimate">
                                        <td style="text-align: left;">
                                            <b>Nama Penerima : </b>
                                            <br>
                                            {{ $alamat->profile->nama }}
                                            <br>
                                            <b>Alamat Penerima : </b>
                                            <br>
                                            {{ $alamat->profile->alamat }} {{ $alamat->profile->kecamatan}} {{ $alamat->profile->kode_pos }}
                                            <br>
                                            <b>Handhone Penerima : </b>
                                            <br>
                                            {{ $alamat->profile->no_telepon }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table><!-- End .table table-summary -->
                            @if ($orders->first()->status == 'Menunggu')
                            <h3 class="summary-title" style="text-align: center;"><b>PEMBAYARAN</b></h3>
                            <p class="summary-title">Selesaikan Pembayaran Dalam : <span id="countdown"></span></p>
                            <a href="{{ route('checkout') }}" id="pay-button" class="btn btn-outline-primary-2 btn-order btn-block">CHECKOUT</a>
                            <a href="{{ route('cancel-order', $orders[0]->no_pesanan) }}" id="cancel" data-confirm-delete="true" class="btn btn-primary btn-order btn-block">BATALKAN
                                PESANAN</a>
                            @elseif($orders->first()->status == 'Dikemas')
                            <h3 class="summary-title" style="text-align: center;"><b>INFORMASI PESANAN</b></h3>
                            <p class="summary-title">Pesanan Sedang Diproses</p>
                            @elseif($orders->first()->status == 'Dikirim')
                            <h3 class="summary-title" style="text-align: center;"><b>INFORMASI PESANAN</b></h3>
                            <p class="summary-title">Pesanan Dalam Pengiriman, Selesaikan Pesanan Jika Barang Telah Diterima</p>
                            <a href="{{ route('finish.order', $orders->first()->no_pesanan) }}"class="btn btn-primary btn-order btn-block">PESANAN DITERIMA</a>
                            @elseif($orders->first()->status == 'Diterima')
                            <h3 class="summary-title" style="text-align: center;"><b>INFORMASI PESANAN</b></h3>
                            <p class="summary-title">Pesanan Telah Diterima Pada Tanggal {{ $orders->first()->updated_at }}</p>
                            <h3 class="summary-title" style="text-align: center;"><b>TAMBAH ULASAN PRODUK</b></h3>
                            @foreach($ordersByStore as $storeName => $orders)
                                @foreach ($orders as $order)
                                @php
                                $review = $reviews->get($order->produk->id);
                                @endphp
                
                            @if($review)
                            <div class="review">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <div class="product-nav product-nav-thumbs">                    
                                            <a href="#" class="active">
                                                <img src="{{ asset($order->produk->images->first()->images) }}" alt="product desc">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <b>{{ $order->produk->nama }}</b> <br>
                                        Rp. {{ number_format($order->produk->harga_netto, 0, ',', '.') }} X {{ $order->qty }}
                                    </div>
                                </div>
                                <form action="{{ route('produk.review.update', $review->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="rating">Rating:</label>
                                        <div class="rating">
                                            <input type="radio" name="rating" value="5" id="5_{{ $order->produk->id }}" {{ $review->rating == 5 ? 'checked' : '' }}>
                                            <label for="5_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="4" id="4_{{ $order->produk->id }}" {{ $review->rating == 4 ? 'checked' : '' }} >
                                            <label for="4_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="3" id="3_{{ $order->produk->id }}" {{ $review->rating == 3 ? 'checked' : '' }}>
                                            <label for="3_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="2" id="2_{{ $order->produk->id }}" {{ $review->rating == 2 ? 'checked' : '' }}>
                                            <label for="2_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="1" id="1_{{ $order->produk->id }}" {{ $review->rating == 1 ? 'checked' : '' }}>
                                            <label for="1_{{ $order->produk->id }}" class="fas fa-star"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="review">Tulis Ulasan:</label>
                                        <textarea name="review" class="form-control" rows="3" required>{{ $review->review }}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Update Ulasan</button>
                                </form>
                            </div>
                            @else
                            <div class="review">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <div class="product-nav product-nav-thumbs">                    
                                            <a href="#" class="active">
                                                <img src="{{ asset($order->produk->images->first()->images) }}" alt="product desc">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <b>{{ $order->produk->nama }}</b> <br>
                                        Rp. {{ number_format($order->produk->harga_netto, 0, ',', '.') }} X {{ $order->qty }}
                                    </div>
                                </div>
                                <form action="{{ route('produk.review.store', $order->produk->id) }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="rating">Rating:</label>
                                        <div class="rating">
                                            <input type="radio" name="rating" value="5" id="5_{{ $order->produk->id }}" >
                                            <label for="5_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="4" id="4_{{ $order->produk->id }}" >
                                            <label for="4_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="3" id="3_{{ $order->produk->id }}" >
                                            <label for="3_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="2" id="2_{{ $order->produk->id }}" >
                                            <label for="2_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            <input type="radio" name="rating" value="1" id="1_{{ $order->produk->id }}" >
                                            <label for="1_{{ $order->produk->id }}" class="fas fa-star"></label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="review">Tulis Ulasan:</label>
                                        <textarea name="review" class="form-control" rows="3" required></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Kirim Ulasan</button>
                                </form>
                            </div>
                            @endif
                                {{-- <div class="review">
                                    <div class="row">
                                        <div class="col-sm-1">
                                            <div class="product-nav product-nav-thumbs">                    
                                                <a href="#" class="active">
                                                    <img src="{{ asset($order->produk->images->first()->images) }}" alt="product desc">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <b>{{ $order->produk->nama }}</b> <br>
                                            Rp. {{ number_format($order->produk->harga_netto, 0, ',', '.') }} X {{ $order->qty }}
                                        </div>
                                    </div>
                                    <form action="{{ route('produk.review.store', $order->produk->id) }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="rating">Rating:</label>
                                            <div class="rating">
                                                <input type="radio" name="rating" value="5" id="5_{{ $order->produk->id }}" >
                                                <label for="5_{{ $order->produk->id }}" class="fas fa-star"></label>
                                                <input type="radio" name="rating" value="4" id="4_{{ $order->produk->id }}" >
                                                <label for="4_{{ $order->produk->id }}" class="fas fa-star"></label>
                                                <input type="radio" name="rating" value="3" id="3_{{ $order->produk->id }}" >
                                                <label for="3_{{ $order->produk->id }}" class="fas fa-star"></label>
                                                <input type="radio" name="rating" value="2" id="2_{{ $order->produk->id }}" >
                                                <label for="2_{{ $order->produk->id }}" class="fas fa-star"></label>
                                                <input type="radio" name="rating" value="1" id="1_{{ $order->produk->id }}" >
                                                <label for="1_{{ $order->produk->id }}" class="fas fa-star"></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="review">Tulis Ulasan:</label>
                                            <textarea name="review" class="form-control" rows="3" required></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Kirim Ulasan</button>
                                        {{-- @if(!in_array($order->produk->id, $reviewedProductIds))
                                        @endif 
                                    </form>
                                </div> --}}
                                @endforeach
                            @endforeach
                            @endif
                        </div><!-- End .summary -->
                    </aside><!-- End .col-lg-3 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .cart -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
<script src="https://app.midtrans.com/snap/snap.js" data-client-key="{{ env('MIDTRANS_CLIENT_KEY') }}"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', (event) => {
        document.querySelectorAll('.rating input[type="radio"]').forEach(input => {
            input.addEventListener('change', function() {
                // Reset semua label rating menjadi abu-abu
                const allLabels = this.parentElement.querySelectorAll('label');
                allLabels.forEach(lbl => lbl.style.color = '#ddd');
                
                // Ubah warna label rating yang dipilih dan di sekitarnya
                let clicked = false;
                allLabels.forEach(lbl => {
                    if (lbl.getAttribute('for') === this.id) clicked = true;
                    if (clicked) lbl.style.color = '#ffcc00';
                });
            });
        });
    });
      document.getElementById('pay-button').onclick = function(){
        event.preventDefault();
        // Swal.fire({
        //         icon: 'success',
        //         title: 'Success !',
        //         text: 'Pembayaran Berhasil.',
        //         confirmButtonText: 'Lihat Pesanan'
        //     }).then((result) => {
        //         window.location.href = '{{ route('checkout-success', $orders[0]->no_pesanan) }}';
        //     });
        // SnapToken acquired from previous step
        snap.pay('{{ $orders[0]->snap_token }}', {
          // Optional
          onSuccess: function(result){
            Swal.fire({
                icon: 'success',
                title: 'Success !',
                text: 'Pembayaran Berhasil.',
                showCancelButton: true,
                confirmButtonText: 'Lihat Pesanan'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('checkout-success', $orders[0]->no_pesanan) }}';
                }
            });
            // /* You may add your own js here, this is just example */ document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
          },
          // Optional
          onPending: function(result){
            /* You may add your own js here, this is just example */ document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
          },
          // Optional
          onError: function(result){
            /* You may add your own js here, this is just example */ document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
          }
        });
      };

      document.getElementById('cancel').onclick = function(){
        event.preventDefault();
        Swal.fire({
                icon: 'warning',
                title: 'Batalkan Pesanan !',
                text: 'Batalkan Pesanan?.',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('cancel-order', $orders[0]->no_pesanan) }}';
                }
            });
    }

      document.addEventListener('DOMContentLoaded', (event) => {
            var countDownDate = new Date("{{ $expiresAt }}").getTime();
            var orderId = {{ $order->id }};
            var x = setInterval(function() {
                var now = new Date().getTime();
                var distance = countDownDate - now;

                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                document.getElementById("countdown").innerHTML = minutes + "m " + seconds + "s ";

                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("countdown").innerHTML = "EXPIRED";
                    deleteOrder(orderId);
                }
            }, 1000);

            function deleteOrder(orderId) {
                fetch(`/orders/{{ $orders->first()->no_pesanan }}}`, {
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        'Content-Type': 'application/json'
                    }
                })
                .then(response => response.json())
                .then(data => {
                    console.log(data.message);
                    window.location.href = '{{ route('cancel-order', $orders[0]->no_pesanan) }}';
                })
                .catch(error => console.error('Error:', error));
            }
        });
    </script>
@endsection
