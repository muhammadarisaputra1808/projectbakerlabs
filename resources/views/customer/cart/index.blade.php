@extends('layouts.user.main')
@section('content')
<main class="main">
    <div class="page-header text-center" style="background-image: url('{{ asset('img/banner-title.jpg') }}')">
        <div class="container">
            <h1 class="page-title">Keranjang<span>Shop</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Keranjang</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->
    <div class="page-content">
        <div class="cart">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <table class="table table-cart table-mobile">
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                @if (!$carts->isEmpty())
                                @foreach($carts as $cart)
                                <tr>
                                    <td class="product-col">
                                        <div class="product">
                                            <figure class="product-media">
                                                @if ($cart->produk->images->isEmpty())
                                                    <img src="{{ asset('img/default.jpg') }}" alt="Product image" class="product-image">    
                                                @else
                                                    <img src="{{ asset($cart->produk->images->first()->images) }}" alt="Product image" class="product-image">    
                                                @endif
                                            </figure><!-- End .product-main-image -->

                                            <h3 class="product-title">
                                                <a href="#">{{ $cart->produk->nama }}</a>
                                                <p>
                                                    {{ $cart->produk->toko->nama }}
                                                </p>
                                            </h3><!-- End .product-title -->
                                        </div><!-- End .product -->
                                    </td>
                                    <td class="price-col">Rp. {{ number_format($cart->produk->harga_netto, 0, ',', '.') }}</td>
                                    <td class="quantity-col">
                                        <div class="cart-product-quantity">
                                            <form id="update-cart-{{ $cart->produk->id }}" action="{{ route('update-cart', $user->id) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="produk_id" value="{{ $cart->produk->id }}">
                                            <input type="number" class="form-control" name="qty" value="{{ $cart->qty }}" min="1" max="{{ $cart->produk->stok }}" step="1" data-decimals="0" required onchange="updateCart({{ $cart->produk->id }})">
                                            </form>
                                        </div><!-- End .cart-product-quantity -->
                                    </td>
                                    <td class="total-col">Rp. {{ number_format($cart->total, 0, ',', '.') }}</td>
                                    <td class="remove-col">
                                        <button class="btn-remove" id="delete-cart" onclick="deleteCart({{ $cart->id }})"><i class="icon-close"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="4" class="text-center">
                                        <i class="fa-solid fa-cart-shopping"></i> <br>
                                        <p>Belum Ada Produk Yang Ditambahkan</p>
                                        <a href="{{ url('/menu') }}" class="btn btn-outline-dark-2 btn-block mb-3"><span>LANJUTKAN BERBELANJA </span><i class="icon-refresh"></i></a>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table><!-- End .table table-wishlist -->
                    </div><!-- End .col-lg-9 -->

                    <!-- Summary section -->
                    <aside class="col-lg-3">
                        <div class="summary summary-cart">
                            <h3 class="summary-title">Total Keranjang</h3><!-- End .summary-title -->

                            <table class="table table-summary">
                                <tbody>
                                    <tr class="summary-subtotal">
                                        <td style="width: 30%;"><strong>Subtotal:</strong></td>
                                        <td style="width: 70%;">Rp. {{ number_format($total_subtotal, 0, ',', '.') }}</td>
                                    </tr><!-- End .summary-subtotal -->
                                </tbody>
                            </table><!-- End .table table-summary -->
                            <h3 class="summary-title mt-2" style="text-align: center;"><b>DATA PENGIRIMAN</b></h3>
                            <table class="table table-summary mb-2">
                                <tbody>
                                    <tr class="summary-shipping-estimate">
                                        <td style="text-align: left;">
                                            <b>Nama Penerima : </b>
                                            <br>
                                            @if($alamat->profile && $alamat->profile->nama)
                                                {{ $alamat->profile->nama }}
                                            @else
                                                <span>-</span>
                                            @endif
                                            <br>
                                            <b>Alamat Penerima : </b>
                                            <br>
                                            @if($alamat->profile && $alamat->profile->alamat && $alamat->profile->kecamatan && $alamat->profile->kode_pos)
                                                {{ $alamat->profile->alamat }} {{ $alamat->profile->kecamatan}} {{ $alamat->profile->kode_pos }}
                                            @else
                                                <span>-</span>
                                            @endif
                                            <br>
                                            <b>Handphone Penerima : </b>
                                            <br>
                                            @if($alamat->profile && $alamat->profile->no_telepon)
                                                {{ $alamat->profile->no_telepon }}
                                            @else
                                                <span class="mb-2">-</span><br>
                                                <a href="{{ route('profile.alamat.show') }}">Perbarui Alamat <i class="icon-edit"></i></a></p>

                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table><!-- End .table table-summary -->
                            
                            <button id="checkout-button" class="btn btn-outline-primary-2 btn-order btn-block">CHECKOUT</button>
                        </div><!-- End .summary -->

                        <a href="{{ url('/menu') }}" class="btn btn-outline-dark-2 btn-block mb-3"><span>LANJUTKAN BERBELANJA </span><i class="icon-refresh"></i></a>
                    </aside><!-- End .col-lg-3 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- End .cart -->
    </div><!-- End .page-content -->
</main><!-- End .main -->

<script>
    document.getElementById('checkout-button').addEventListener('click', function(event) {
        event.preventDefault();
        
        @if($carts->isEmpty())
            Swal.fire({
                icon: 'warning',
                title: 'Maaf !',
                text: 'Tidak ada produk di dalam keranjang.',
            });
        @elseif($alamat->profile == null)
            Swal.fire({
                icon: 'info',
                title: 'Maaf !',
                text: 'Alamat Pengiriman Belum Ditambahkan.',
                showCancelButton: true,
                confirmButtonText: 'Atur Alamat',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ route('profile.alamat.show') }}';
                }
            });
        @else
            window.location.href = '{{ route('checkout') }}';
        @endif
    });

    function updateCart(productId) {
    var form = $('#update-cart-' + productId);
    var data = form.serialize();
    var url = form.attr('action');
    
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function(response) {
            // Handle success - update the cart total and any other necessary UI elements
            console.log(response);
        },
        error: function(xhr, status, error) {
            // Handle error
            console.error(xhr.responseText);
        }
    });
}
    function deleteCart(cartId) {
        event.preventDefault()
    
        var url = '{{ route("remove-from-cart", ":id") }}';
        url = url.replace(':id', cartId);

        Swal.fire({
                icon: 'warning',
                title: 'Hapus Dari Keranjang! ',
                text: 'Apakah Anda yakin ingin menghapus produk ini dari keranjang?',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            });
    }
</script>
@endsection