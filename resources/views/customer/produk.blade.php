@extends('layouts.user.main')
@section('content')
<main class="main">
    <div class="page-header text-center" style="background-image: url({{ asset('img/banner-title.jpg') }})">
        <div class="container">
            <h1 class="page-title">Detail<span>Produk</span></h1>
        </div><!-- End .container -->
    </div><!-- End .page-header -->
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/menu') }}">Produk</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
            </ol>
        </div><!-- End .container -->
    </nav><!-- End .breadcrumb-nav -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="product-details-top">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="product-gallery product-gallery-horizontal">
                                    <div class="row">
                                        <figure class="product-main-image">
                                            @if ($produk->images->isEmpty())
                                                <img id="main-product-image" src="{{ asset('img/default.jpg') }}" alt="Product image" class="product-image" width="100px" height="100px">    
                                            @else
                                                <img id="main-product-image" src="{{ asset($produk->images->first()->images) }}" alt="Product image" class="img-thumbnail img-fluid product-image" width="100%" height="100px">    
                                            @endif
                                        </figure><!-- End .product-main-image -->
                            
                                        <div id="product-zoom-gallery" class="product-image-gallery">
                                            @foreach ($produk->images as $item)    
                                                <a class="product-gallery-item" class="active">
                                                    <img class="img-thumbnail" src="{{ asset($item->images) }}" alt="product desc" width="100px" height="100px">
                                                </a>
                                            @endforeach
                                        </div><!-- End .product-image-gallery -->
                                    </div><!-- End .row -->
                                </div><!-- End .product-gallery -->
                            </div><!-- End .col-md-6 -->

                            <div class="col-md-6">
                                <div class="product-details">
                                    <h1 class="product-title">{{ $produk->nama }}</h1><!-- End .product-title -->
                                    @if ($produk->reviews->isNotEmpty())
                                        @foreach($produk->reviews as $review)
                                            <div class="ratings-container">
                                                <div class="ratings">
                                                    <div class="ratings-val" style="width: {{ ($review->rating / 5) * 100 }}%;"></div><!-- End .ratings-val -->
                                                </div><!-- End .ratings -->
                                                <span class="ratings-text">({{ $review->rating }} / 5)</span>
                                            </div><!-- End .ratings-container -->
                                        @endforeach
                                        @else     
                                        <div class="ratings-container">
                                            <div class="ratings">
                                                <div class="ratings-val" style="width: 0%;"></div><!-- End .ratings-val -->
                                            </div><!-- End .ratings -->
                                            <span class="ratings-text">( 0 / 5 )</span>
                                        </div><!-- End .rating-container -->
                                        @endif
                                    <div class="product-price">
                                        Rp. {{ number_format($produk->harga_netto, 0, ',', '.') }}
                                    </div><!-- End .product-price -->

                                    <div class="product-content">
                                        <p>{{ $produk->deskripsi}}</p>
                                    </div><!-- End .product-content -->
                                    <div class="product-content">
                                        {{ $produk->accepted_orders_count }} Terjual
                                    </div>
                                    <div class="details-filter-row details-row-size">
                                        <label for="qty">Qty:</label>
                                        <div class="product-details-quantity">
                                            <form id="add-to-cart-form-{{ $produk->id }}" action="{{ route('add-to-cart') }}" method="POST">
                                                @csrf
                                            <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                                            <input type="number" id="qty" class="form-control" value="1" min="1" max="{{ $produk->stok }}" step="1" data-decimals="0" required>
                                        </div><!-- End .product-details-quantity -->
                                    </div><!-- End .details-filter-row -->
                                    <div class="details-filter-row details-row-size">
                                        <label for="qty">Stok :</label>
                                        <div class="product-details-quantity">
                                            {{ $produk->stok }}
                                        </div><!-- End .product-details-quantity -->
                                    </div><!-- End .details-filter-row -->
                                    <div>
                                        <a href="#" class="btn btn-primary btn-rounded   mb-2" onclick="event.preventDefault(); document.getElementById('add-to-cart-form-{{ $produk->id }}').submit();">
                                            <span><i class="fa-solid fa-cart-plus"></i>Tambah Ke Keranjang</span>
                                        </a>
                                    </div>
                                    <div class="product-details-footer">
                                        <div class="product-cat">
                                            <span>Kategori :</span>
                                            <a href="#">{{ $produk->kategori->nama }}</a>
                                        </div><!-- End .product-cat -->
                                    </div><!-- End .product-details-footer -->
                                </div><!-- End .product-details -->
                            </div><!-- End .col-md-6 -->
                        </div><!-- End .row -->
                    </div><!-- End .product-details-top -->
                    <div class="product-details-tab">
                        <ul class="nav nav-pills justify-content-center" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="product-info-link" data-toggle="tab" href="#product-info-tab" role="tab" aria-controls="product-info-tab" aria-selected="false">Additional information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="product-review-link" data-toggle="tab" href="#product-review-tab" role="tab" aria-controls="product-review-tab" aria-selected="false">Reviews {{ $produk->reviews->count() }} </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="product-info-tab" role="tabpanel" aria-labelledby="product-info-link">
                                <div class="product-desc-content">
                                    <h3>Informasi Toko :</h3>
                                    <div class="row">
                                        <div class="col-2">
                                            <figure class="product-media mb-3" style="width: 100px">
                                                <img src="{{ asset($toko->image) }}" alt="Toko image" class="product-image">
                                            </figure>
                                        </div>
                                        <div class="col-auto">
                                            <h3><i class="fa-solid fa-store"></i><b>  {{$toko->nama}}</b></h3>
                                            <p><i class="fa-solid fa-location-dot"></i>  {{$toko->alamat}}</p>
                                            <p><i class="fa-solid fa-square-phone"></i>  <a href="https://wa.me/{{$toko->no_telepon_for_url}}?text=Halo%20apakah%20stok%20produk%20{{ $produk->nama }}%20masih%20ada%20di%20BakerLabs%20%3F">{{$toko->no_telepon}}</a></p>
                                        </div>
                                    </div>
                                </div><!-- End .product-desc-content -->
                            </div><!-- .End .tab-pane -->
                            <div class="tab-pane fade" id="product-review-tab" role="tabpanel" aria-labelledby="product-review-link">
                                <div class="reviews">
                                    <h3>Reviews {{ $produk->reviews->count() }}</h3>
                                    <div class="review">
                                        @if ($produk->reviews->isNotEmpty())
                                            @foreach($produk->reviews as $review)
                                            <div class="row no-gutters">
                                                <div class="col-auto">
                                                    <h4><a href="#">{{ $review->user->name }}</a></h4>
                                                    <div class="review-content">
                                                        <p>{{ $review->review }}</p>
                                                    </div><!-- End .review-content -->
                                                    <div class="ratings-container">
                                                        <div class="ratings">
                                                            <div class="ratings-val" style="width: {{ ($review->rating / 5) * 100 }}%;"></div><!-- End .ratings-val -->
                                                        </div><!-- End .ratings -->
                                                    </div><!-- End .rating-container -->
                                                    <span class="review-date">{{ $review->created_at->diffForHumans() }}</span>
                                                </div><!-- End .col -->
                                            </div><!-- End .row -->
                                            @endforeach
                                        @else
                                            <p>Belum Ada Ulasan Yang Ditambahkan</p>
                                        @endif
                                    </div><!-- End .review -->
                                </div><!-- End .reviews -->
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .product-details-tab -->
                </div>

                <aside class="col-lg-3 order-lg-first">
                    <div class="sidebar sidebar-shop">
                        {{-- <div class="widget widget-clean">
                            <label>Filters:</label>
                            <a href="#" class="sidebar-filter-clear">Clean All</a>
                        </div><!-- End .widget widget-clean --> --}}

                        <div class="widget widget-collapsible">
                            <h3 class="widget-title">
                                <a data-toggle="collapse" href="#widget-1" role="button" aria-expanded="true"
                                    aria-controls="widget-1">
                                    Category
                                </a>
                            </h3><!-- End .widget-title -->

                            <div class="collapse show" id="widget-1">
                                <div class="widget-body">
                                    <div class="filter-items">
                                        <div class="filter-item">
                                            @foreach ($kategori as $item)
                                            <div class="mx-3">
                                                <a href="{{ url('/menu/'.$item->slug ) }}">{{ $item->nama }}</a>
                                            </div><!-- End .custom-checkbox -->
                                            @endforeach
                                        </div><!-- End .filter-item -->
                                    </div><!-- End .filter-items -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .collapse -->
                        </div><!-- End .widget -->
                    </div><!-- End .sidebar sidebar-shop -->
                </aside><!-- End .col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .page-content -->
</main><!-- End .main -->
<script>
    document.addEventListener('DOMContentLoaded', function() {
    const mainImage = document.getElementById('main-product-image');
    const galleryItems = document.querySelectorAll('.product-gallery-item img');

    galleryItems.forEach(item => {
        item.addEventListener('click', function(event) {
            event.preventDefault();
            mainImage.src = this.src;
        });
    });
});
</script>
@endsection