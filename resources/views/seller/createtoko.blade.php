@extends('layouts.user.main')
@section('content')
<main class="main">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="form-box mt-3 mb-3">
                    <div class="form-tab">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <ul class="nav nav-pills nav-fill nav-border-anim" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="signin-tab" data-toggle="tab" href="#signin" role="tab"
                                    aria-controls="signin" aria-selected="true">Daftar Toko</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="tab-content-5">
                            <div class="tab-pane fade show active" id="signin" role="tabpanel"
                                aria-labelledby="signin-tab">
                                <form action="{{ url('daftartoko') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama">Nama Toko *</label>
                                        <input id="nama" type="text"
                                            class="form-control @error('nama') is-invalid @enderror" name="nama"
                                            value="{{ old('nama') }}"  autocomplete="nama" autofocus
                                            placeholder="{{ __('Atur Nama Toko') }}">
                                        @error('nama')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div><!-- End .form-group -->
                                    <div class="form-group">
                                        <label for="no_telepon">No. Telepon *</label>
                                        <input id="no_telepon" type="text"
                                            class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon"
                                            value="{{ old('no_telepon') }}"  autocomplete="no_telepon" autofocus
                                            placeholder="{{ __('Atur No Telepon') }}">
                                        @error('no_telepon')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div><!-- End .form-group -->
                                    <div class="form-group">
                                        <label for="kecamatan">Kecamatan *</label>
                                        <select name="kecamatan" id="kecamatan" class="form-control @error('kecamatan') is-invalid @enderror" value="{{ old('kecamatan') }}">
                                            <option value="">-- Pilih Kecamatan --</option>
                                            @foreach ($kecamatan as $item)
                                            <option value="{{ $item }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                        @error('kecamatan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div><!-- End .form-group -->

                                    <div class="form-group">
                                        <label for="alamat">Alamat *</label>
                                        <input id="alamat" type="text"
                                            class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                                             autocomplete="current-alamat" placeholder="{{ __('Atur Alamat') }}">
                                        @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div><!-- End .form-group -->
                                    <div class="form-group">
                                        <label for="kode_pos">Kode Pos *</label>
                                        <input id="kode_pos" type="text"
                                            class="form-control @error('kode_pos') is-invalid @enderror" name="kode_pos"
                                             autocomplete="current-kode_pos" placeholder="{{ __('Atur Kode Pos') }}">
                                        @error('kode_pos')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div><!-- End .form-group -->
                                    <div class="form-group">
                                        <label for="image" class="form-label">Upload Photo / Logo * </label>
                                        <input type="file" name="image" id="image" class="form-control"> 
                                    </div>
                                    <div class="form-footer"
                                        style="display: flex; flex-direction: column; align-items: center;">
                                        <div style="display: flex; flex-direction: column; align-items: center;">
                                            <button type="submit" class="btn btn-primary btn-rounded">Buat Toko <i class="icon-long-arrow-right"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .form-tab -->
                </div><!-- End .form-box -->
            </div>
        </div>
    </div>

</main><!-- End .main -->
@endsection