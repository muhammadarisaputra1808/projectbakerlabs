@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Edit Profil
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Profil Toko</h3>
                        <div class="card-actions">
                            <a href="{{ route('dashboard') }}" class="btn btn-primary">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-arrow-left">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M5 12l14 0"></path>
                                    <path d="M5 12l6 6"></path>
                                    <path d="M5 12l6 -6"></path>
                                </svg>
                                Kembali
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <span class="avatar avatar-xl">
                                    <img id="preview" src="{{ asset($toko->image ?? 'img/default.jpg') }}" alt="image" style="max-height: 200px">
                                </span>
                            </div>
                            <form class="col-auto" action="{{ route('profile.photo.toko.update', $toko->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-auto">
                                        <input type="file" name="image" id="file" class="d-none" onchange="previewImage(event)">
                                        <label for="file" class="btn btn-primary m-0">Ganti Photo<i class="fa-regular fa-image"></i></label>
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary">Perbarui<i class="fa-regular fa-pen-to-square"></i></button>
                                    </div>
                                </div>
                            </form>
                          </div>
                        <form action="{{ route('update-toko', $toko->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="mb-3 row">
                                <label class="col-sm-2 col-form-label required">Nama Toko : </label>
                                <div class="col">
                                    <input type="text" name="nama"
                                        class="form-control @error('nama') is-invalid @enderror"
                                        placeholder="Atur Nama Produk" value="{{ $toko->nama }}">
                                    @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="no_telepon" class="col-form-label required">No Whatsapp / Telepon </label>
                                        <input id="no_telepon" type="text" class="form-control @error('no_telepon') is-invalid @enderror" name="no_telepon" value="{{ $toko->no_telepon }}" autocomplete="no_telepon" placeholder="Masukkan Nomor Telepon ...">
                                        @error('no_telepon')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        {{-- <div class="alert alert-info" role="alert">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 9h.01"></path><path d="M11 12h1v4h1"></path></svg>
                                            <strong>Notifikasi jika ada pesanan / transaksi lainnya akan dikirim ke nomor ini</strong>
                                        </div>   --}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="kecamatan" class="col-form-label required">Kecamatan</label>
                                        <select name="kecamatan" id="kecamatan" class="form-select @error('kecamatan') is-invalid @enderror">
                                            <option value="">-- Pilih Kecamatan --</option>
                                            @foreach ($kecamatan as $item)
                                            <option value="{{ $item }}" {{ $toko->kecamatan == $item ? 'selected' : ''}}>{{ $item }}</option>
                                            @endforeach
                                        </select>
                                        @error('kecamatan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-2 col-form-label required">Alamat :</label>
                                <div class="col">
                                    <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ $toko->alamat }}" autocomplete="alamat" placeholder="Masukkan Alamat Lengkap ...">
                                    @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-2 col-form-label required">Kode_pos :</label>
                                <div class="col-sm-2">
                                    <input id="kode_pos" type="text" class="form-control @error('kode_pos') is-invalid @enderror" name="kode_pos" value="{{ $toko->kode_pos }}" autocomplete="kode_pos" placeholder="Masukkan kode_pos Lengkap ...">
                                    @error('kode_pos')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                    </div>
                    <div class="card-footer text-end">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function addImageInput() {
        var imageInputs = document.getElementById('imageInputs');
        var newImageInput = document.createElement('div');
        newImageInput.className = 'col';
        newImageInput.innerHTML = `
            <div class="col">
                <small class="form-hint">
                                        Gambar Tambahan 
                                    </small>
                <input type="file" class="form-control @error('images') is-invalid @enderror" name="images[]">
            </div>
        `;
        imageInputs.appendChild(newImageInput);
    }
    function previewImage(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('preview');
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}
</script>