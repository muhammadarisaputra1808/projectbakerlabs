@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Dashboard Toko
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Saldo Anda : Rp. {{ number_format($saldo, 0, ',', '.')   }}</h3>
                    </div>
                    <div class="card-body">
                       <div class="row row-cards">
                        <div class="col-md-8">
                            <div class="card">
                              <div class="card-header">
                                <ul class="nav nav-tabs card-header-tabs nav-fill" data-bs-toggle="tabs" role="tablist">
                                  <li class="nav-item" role="presentation">
                                    <a href="#tabs-home-7" class="nav-link active" data-bs-toggle="tab" aria-selected="true" role="tab"><!-- Download SVG icon from http://tabler-icons.io/i/home -->
                                        <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-credit-card-pay me-2"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 19h-6a3 3 0 0 1 -3 -3v-8a3 3 0 0 1 3 -3h12a3 3 0 0 1 3 3v4.5" /><path d="M3 10h18" /><path d="M16 19h6" /><path d="M19 16l3 3l-3 3" /><path d="M7.005 15h.005" /><path d="M11 15h2" /></svg>
                                       Nomor Rekening</a>
                                  </li>
                                  <li class="nav-item" role="presentation">
                                    <a href="#tabs-profile-7" class="nav-link" data-bs-toggle="tab" aria-selected="false" tabindex="-1" role="tab"><!-- Download SVG icon from http://tabler-icons.io/i/user -->
                                        <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-credit-card-refund me-2"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 19h-6a3 3 0 0 1 -3 -3v-8a3 3 0 0 1 3 -3h12a3 3 0 0 1 3 3v4.5" /><path d="M3 10h18" /><path d="M7 15h.01" /><path d="M11 15h2" /><path d="M16 19h6" /><path d="M19 16l-3 3l3 3" /></svg>
                                      Tarik Saldo</a>
                                  </li>
                                  <li class="nav-item" role="presentation">
                                    <a href="#tabs-activity-7" class="nav-link" data-bs-toggle="tab" aria-selected="false" tabindex="-1" role="tab"><!-- Download SVG icon from http://tabler-icons.io/i/activity -->
                                      <svg xmlns="http://www.w3.org/2000/svg" class="icon me-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12h4l3 8l4 -16l3 8h4"></path></svg>
                                      Riwayat</a>
                                  </li>
                                </ul>
                              </div>
                              <div class="card-body">
                                <div class="tab-content">
                                  <div class="tab-pane active show" id="tabs-home-7" role="tabpanel">
                                    <div class="alert alert-important alert-info alert-dismissible" role="alert">
                                        <div class="d-flex">
                                          <div>
                                            <!-- Download SVG icon from http://tabler-icons.io/i/info-circle -->
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 9h.01"></path><path d="M11 12h1v4h1"></path></svg>
                                          </div>
                                          <div>
                                            Rekening di bawah ini, akan digunakan untuk pencairan transaksi Toko Anda.
                                          </div>
                                        </div>
                                      </div>
                                      @if ($toko->nomor_rekening == null)
                                      <div class="alert alert-important alert-warning alert-dismissible" role="alert">
                                        <div class="d-flex">
                                          <div>
                                            <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 9v4"></path><path d="M10.363 3.591l-8.106 13.534a1.914 1.914 0 0 0 1.636 2.871h16.214a1.914 1.914 0 0 0 1.636 -2.87l-8.106 -13.536a1.914 1.914 0 0 0 -3.274 0z"></path><path d="M12 16h.01"></path></svg>
                                          </div>
                                          <div>
                                            Anda belum menentukan rekening, silahkan isi form dibawah ini.
                                          </div>
                                        </div>
                                      </div>
                                      <form action="{{ route('update.no.rekening', $toko->id) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                        <div class="mb-3">
                                            <div class="form-label">Bank :</div>
                                            <input type="text" class="form-control @error('bank') is-invalid @enderror" name="bank" placeholder="Masukkan Nama Bank ...">
                                            @error('bank')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-label">Nomor Rekening :</div>
                                            <input type="text" class="form-control @error('nomor_rekening') is-invalid @enderror" name="nomor_rekening" placeholder="Masukkan Nomor Rekening ...">
                                            @error('nomor_rekening')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-label">Nama Pemilik :</div>
                                            <input type="text" class="form-control @error('nama_pemilik') is-invalid @enderror" name="nama_pemilik" placeholder="Masukkan Nama Pemilik ...">
                                            @error('nama_pemilik')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <small class="form-hint mb-3">Pastikan Anda Memasukkan Data Dengan Benar Agar Tidak Terjadi Kesalahan Pengiriman.</small>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                      </form>
                                      @else
                                      <form action="{{ route('update.no.rekening', $toko->id) }}" method="post">
                                        @csrf
                                        @method('PUT')
                                      
                                        <div class="mb-3">
                                            <div class="form-label">Bank :</div>
                                            <input type="text" class="form-control @error('bank') is-invalid @enderror" name="bank" placeholder="Masukkan Nama Bank ..." value="{{ $toko->bank }}">
                                            @error('bank')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-label">Nomor Rekening :</div>
                                            <input type="text" class="form-control @error('nomor_rekening') is-invalid @enderror" name="nomor_rekening" placeholder="Masukkan Nomor Rekening ..." value="{{ $toko->nomor_rekening }}">
                                            @error('nomor_rekening')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <div class="form-label">Nama Pemilik :</div>
                                            <input type="text" class="form-control @error('nama_pemilik') is-invalid @enderror" name="nama_pemilik" placeholder="Masukkan Nama Pemilik ..." value="{{ $toko->nama_pemilik }}">
                                            @error('nama_pemilik')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-primary">Perbarui</button>
                                        </form>
                                      @endif
                                  </div>
                                  <div class="tab-pane" id="tabs-profile-7" role="tabpanel">
                                    @if ($toko->nomor_rekening != null)
                                    <div class="alert alert-important alert-info alert-dismissible" role="alert">
                                        <div class="d-flex mb-2">
                                          <div>
                                            <!-- Download SVG icon from http://tabler-icons.io/i/info-circle -->
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 9h.01"></path><path d="M11 12h1v4h1"></path></svg>
                                          </div>
                                          <div>
                                            Proses transfer ke rekening bank 1x24 jam karena akan diverifikasi terlebih dahulu oleh team BakerLabs
                                          </div>
                                        </div>
                                        <div class="d-flex mb-2">
                                          <div>
                                            <!-- Download SVG icon from http://tabler-icons.io/i/info-circle -->
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 9h.01"></path><path d="M11 12h1v4h1"></path></svg>
                                          </div>
                                          <div>
                                            Pastikan bank, nomor rekening, nama telah benar
                                          </div>
                                        </div>
                                        <div class="d-flex mb-2">
                                          <div>
                                            <!-- Download SVG icon from http://tabler-icons.io/i/info-circle -->
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 9h.01"></path><path d="M11 12h1v4h1"></path></svg>
                                          </div>
                                          <div>
                                            Maksimum tarik/kirim saldo Rp 1.000.000
                                          </div>
                                        </div>
                                      </div>
                                    <h4>Informasi Rekening Tujuan</h4>
                                    <div class="mb-3">
                                        <div class="form-label">Bank :</div>
                                        <input type="text" class="form-control @error('bank') is-invalid @enderror" disabled name="bank" placeholder="Masukkan Nama Bank ..." value="{{ $toko->bank }}">
                                        @error('bank')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <div class="form-label">Nomor Rekening :</div>
                                        <input type="text" class="form-control @error('nomor_rekening') is-invalid @enderror" disabled name="nomor_rekening" placeholder="Masukkan Nomor Rekening ..." value="{{ $toko->nomor_rekening }}">
                                        @error('nomor_rekening')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <div class="form-label">Nama Pemilik :</div>
                                        <input type="text" class="form-control @error('nama_pemilik') is-invalid @enderror" disabled name="nama_pemilik" placeholder="Masukkan Nama Pemilik ..." value="{{ $toko->nama_pemilik }}">
                                        @error('nama_pemilik')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <form action="{{ route('simpan.nominal', $toko->id) }}" method="post">
                                        @csrf
                                        <div class="mb-3">
                                            <div class="form-label">Nominal :</div>
                                            <input type="number" class="form-control @error('jumlah') is-invalid @enderror" name="jumlah" placeholder="Masukkan Nominal Penarikan ..." value="{{ old('jumlah') }}">
                                            @error('jumlah')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <small class="form-hint">
                                                Hindari penggunaan simbol titik atau koma, input angka biasa ! contoh : 100000
                                            </small>
                                        </div>
                                        <button class="btn btn-primary" type="submit">Kirim</button>
                                    </form>
                                    @else
                                        
                                    @endif
                                  </div>
                                  <div class="tab-pane" id="tabs-activity-7" role="tabpanel">
                                    <h4>Activity tab</h4>
                                    <div>Donec ac vitae diam amet vel leo egestas consequat rhoncus in luctus amet, facilisi sit mauris accumsan nibh habitant senectus</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection