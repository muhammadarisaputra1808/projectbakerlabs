@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Detail Pesanan
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Pesanan</h3>
                        <div class="card-actions">
                            <a href="{{ route('pesanan') }}" class="btn btn-primary">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-arrow-left">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M5 12l14 0"></path>
                                    <path d="M5 12l6 6"></path>
                                    <path d="M5 12l6 -6"></path>
                                </svg>
                                Kembali
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table table-vcenter card-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Produk</th>
                                            <th>Jumlah Pesanan</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($detailPesanan) == 0)
                                        <tr class="text-center">
                                            <td colspan="8">Belum Ada Produk Yang Ditambahkan</td>
                                        </tr>
                                        @endif
                                        @foreach ($detailPesanan as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->produk->nama }}</td>
                                                <td>{{ $item->qty}}</td>
                                                <td>{{ $item->total}}</td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="accordion mt-3" id="accordion-example">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading-1">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse-1" aria-expanded="false">
                                        <strong>Informasi Pembeli</strong>
                                    </button>
                                </h2>
                                <div id="collapse-1" class="accordion-collapse collapse"
                                    data-bs-parent="#accordion-example" style="">
                                    <div class="accordion-body pt-0">
                                            <div class="mb-3 row">
                                                <label class="col-sm-3 col-form-label">Nama Pembeli </label>
                                                <div class="col">
                                                    <input type="text" name="nama" class="form-control " disabled
                                                        placeholder="Atur Nama Produk" value="{{ $detailPesanan->first()->user->profile->nama }}">
                                                </div>
                                            </div>
                                            <div class="mb-3 row">
                                                <label class="col-sm-3 col-form-label">Nomor WhatsApp/Telepon : </label>
                                                <div class="col">
                                                    <input type="text" name="nama" class="form-control " disabled
                                                        placeholder="Atur Nama Produk" value="{{ $detailPesanan->first()->user->profile->no_telepon }}">
                                                </div>
                                            </div>
                                            <div class="mb-3 row">
                                                <label class="col-sm-3 col-form-label">Alamat Pengiriman</label>
                                                <div class="col">
                                                    <textarea rows="5" class="form-control " name="deskripsi" value="" disabled
                                                        placeholder="Jelaskan detail dari produk Anda, agar pengguna tertarik untuk membeli !">{{ $detailPesanan->first()->user->profile->alamat.' ,'.$detailPesanan->first()->user->profile->getKecamatanFullAttributes().' '.$detailPesanan->first()->user->profile->kode_pos  }}</textarea>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($detailPesanan->first()->status == 'Dikemas')
                    <div class="card-footer text-end">
                        <a href="{{ route('proses-pesanan', $detailPesanan->first()->no_pesanan) }}" class="btn btn-primary">Proses Pesanan</a>
                    </div>
                    @elseif($detailPesanan->first()->status == 'Dikirim')
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-auto">
                              <div class="text-truncate">
                                Pesanan Telah Diproses Dan Akan Dijemput Oleh Kurir Harap Siapkan Pesanan!
                              </div>
                            </div>
                            <div class="col">
                              <div class="badge bg-primary"></div>
                            </div>
                          </div>
                    </div>
                    @elseif($detailPesanan->first()->status == 'Diterima')
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-auto">
                              <div class="text-truncate">
                                Pesanan Telah Diterima Oleh Pelanggan Pada {{ $detailPesanan->first()->updated_at }}!
                              </div>
                            </div>
                            <div class="col">
                              <div class="badge bg-primary"></div>
                            </div>
                          </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection