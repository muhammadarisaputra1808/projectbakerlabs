@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Kelola Produk
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Produk</h3>
                        <div class="card-actions">
                            <a href="{{ url('/toko/produk') }}" class="btn btn-primary">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round"
                                    class="icon icon-tabler icons-tabler-outline icon-tabler-arrow-left">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                    <path d="M5 12l14 0" />
                                    <path d="M5 12l6 6" />
                                    <path d="M5 12l6 -6" />
                                </svg>
                                Lihat Produk
                            </a>
                        </div>
                    </div>
                    <form action="{{ url('/toko/produk/edit/'.$produk->id) }}" method="post"
                        enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="card-body">
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Pilih Kategori </label>
                                <div class="col">
                                    <select class="form-select @error('kategori_id') is-invalid @enderror"
                                        name="kategori_id" value="{{ old('kategori_id') }}">
                                        <option value="">-- Pilih Kategori --</option>
                                        @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}" {{ $produk->kategori_id == $item->id ? 'selected' : ''
                                            }}>{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                    @error('kategori_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Nama Produk </label>
                                <div class="col">
                                    <input type="text" name="nama"
                                        class="form-control @error('nama') is-invalid @enderror"
                                        placeholder="Atur Nama Produk" value="{{ $produk->nama }}">
                                    @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Harga Normal</label>
                                <div class="col">
                                    <input type="number" name="harga_list"
                                        class="form-control @error('harga_list') is-invalid @enderror"
                                        placeholder="Atur Harga Normal" value="{{ $produk->harga_list }}">
                                    <small class="form-hint">
                                        Hindari penggunaan simbol titik atau koma, input angka biasa ! contoh : 100000
                                    </small>
                                    @error('harga_list')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Harga Jual</label>
                                <div class="col">
                                    <input type="number" name="harga_netto"
                                        class="form-control @error('harga_netto') is-invalid @enderror"
                                        placeholder="Atur Harga Jual" value="{{ $produk->harga_netto }}">
                                    <small class="form-hint">
                                        Hindari penggunaan simbol titik atau koma, input angka biasa ! contoh : 100000
                                    </small>
                                    @error('harga_netto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Stok / Jumlah</label>
                                <div class="col">
                                    <input type="number" name="stok"
                                        class="form-control @error('stok') is-invalid @enderror"
                                        placeholder="Atur Stok / Jumlah" value="{{ $produk->stok }}">
                                    @error('stok')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Deskripsi Produk</label>
                                <div class="col">
                                    <textarea rows="5" class="form-control @error('deskripsi') is-invalid @enderror"
                                        name="deskripsi" value="{{ old('deskripsi') }}"
                                        placeholder="Jelaskan detail dari produk Anda, agar pengguna tertarik untuk membeli !">{{ $produk->deskripsi }}</textarea>
                                    @error('deskripsi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label required">Gambar Produk</label>
                                <div class="col" id="imageInputs">
                                    <input type="file" class="form-control @error('images.*') is-invalid @enderror" name="images[]">
                                    @error('images.*')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label"></label>
                                <div class="col">
                                    <button type="button" onclick="addImageInput()" class="btn btn-secondary">Tambah Gambar</button>
                                </div>
                            </div>
                            <div class="row g-3">
                                <label class="col-sm-3 m-0 p-0 col-form-label"></label>
                                @foreach ($produk->images as $item)
                                <div class="col-1" style="width: 92px" id="image-{{ $item->id }}">
                                    <a data-fslightbox="gallery"
                                        href="{{ asset($item->images) }}">
                                        <!-- Photo -->
                                        <div class="img-responsive img-responsive-1x1 rounded border"
                                            style="background-image: url({{ asset($item->images) }})">
                                        </div>
                                    </a>
                                    @if ($produk->images->count() == 1)
                                    @else
                                    <a href="{{ route('delete.image', $item->id) }}" class="btn btn-danger mt-2" data-confirm-delete="true">Hapus</a>
                                    @endif
                                    {{-- <button type="button" onclick="removeExistingImage({{ $item->id }})" class="btn btn-danger mt-2">Hapus</button> --}}
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function addImageInput() {
        var imageInputs = document.getElementById('imageInputs');
        var newImageInput = document.createElement('div');
        newImageInput.className = 'col';
        newImageInput.innerHTML = `
            <div class="col">
                <small class="form-hint">
                                        Gambar Tambahan 
                                    </small>
                <input type="file" class="form-control" name="images[]">
            </div>
        `;
        imageInputs.appendChild(newImageInput);
    }
</script>