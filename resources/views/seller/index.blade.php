@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Dashboard Toko
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Profil Toko</h3>
                        <div class="card-actions">
                            <a href="{{ route('edit-toko') }}" class="btn btn-primary">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" style="margin: 0" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="icon icon-tabler icons-tabler-outline icon-tabler-edit">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1"></path>
                                    <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z">
                                    </path>
                                    <path d="M16 5l3 3"></path>
                                </svg>
                                Perbarui Toko
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-auto"><span class="avatar avatar-xl" style="background-image: url({{ url($toko->image) }})"></span>
                            </div>
                          </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Toko : </label>
                            <div class="col">
                                <input type="text" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror"
                                    placeholder="Atur Nama Produk" value="{{ $toko->nama }}" disabled>
                                @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">WhatsApp / Telepon Toko : </label>
                            <div class="col">
                                <input type="text" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror"
                                    placeholder="Atur Nama Produk" value="{{ $toko->no_telepon }}" disabled>
                                {{-- <div class="alert alert-info" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 9h.01"></path><path d="M11 12h1v4h1"></path></svg>
                                    <strong>Notifikasi jika ada pesanan / transaksi lainnya akan dikirim ke nomor ini</strong>
                                </div>   --}}
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Alamat Lengkap Toko :</label>
                            <div class="col">
                                <textarea rows="5" class="form-control @error('deskripsi') is-invalid @enderror"
                                    name="deskripsi" value="{{ old('deskripsi') }}"
                                    placeholder="Jelaskan detail dari produk Anda, agar pengguna tertarik untuk membeli !" disabled>{{ $toko->alamat }} {{ $toko->getKecamatanFullAttributes()}} {{
                                            $toko->kode_pos }}</textarea>
                                @error('deskripsi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Penjualan : </label>
                            <div class="col">
                                <input type="text" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror"
                                    placeholder="Atur Nama Produk" value="{{ $jumlahPenjualan }}" disabled>
                                @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function addImageInput() {
        var imageInputs = document.getElementById('imageInputs');
        var newImageInput = document.createElement('div');
        newImageInput.className = 'col';
        newImageInput.innerHTML = `
            <div class="col">
                <small class="form-hint">
                                        Gambar Tambahan 
                                    </small>
                <input type="file" class="form-control @error('images') is-invalid @enderror" name="images[]">
            </div>
        `;
        imageInputs.appendChild(newImageInput);
    }
</script>