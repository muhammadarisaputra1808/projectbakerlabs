@extends('layouts.user.main')
@section('content')
<main class="main"> 
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="form-box mt-3 mb-3">
                    <div class="form-tab">
                        <ul class="nav nav-pills nav-fill nav-border-anim" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="signin" aria-selected="true">Register</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="tab-content-5">
                            <div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="signin-tab">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $item)
                                                <li>{{$item}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="{{ route('register') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Username *</label>
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="{{ __('Username') }}">
        
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Email *</label>
                                        <input type="email" class="form-control @error('name') is-invalid @enderror" name="email" value="{{old('email')}}" required placeholder="{{ __('Email') }}">
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div><!-- End .form-group -->
                                    
                                    <div class="form-group">
                                        <label for="password">Password *</label>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div><!-- End .form-group -->
                                    <div class="form-group">
                                        <label for="password">Confirm Password *</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                                    </div><!-- End .form-group -->
                                    <div class="form-footer p-0 mb-1" style="display: flex; flex-direction: column; align-items: center;">
                                        <button type="submit" class="btn btn-outline-primary-2" name="submit" style="margin: 5px 0;">
                                            <span>Daftar</span>
                                            <i class="icon-long-arrow-right"></i>
                                        </button>
                                        <a href="#" class="forgot-link" style="margin: 5px 0;">Sudah Punya Akun ?</a>
                                    </div><!-- End .form-footer -->
                                    <div style="display: flex; flex-direction: column; align-items: center;">
                                        <a href="{{ route('login') }}" class="btn btn-primary mx-1"><span>Masuk</span> <i class="icon-long-arrow-right"></i></a>
                                    </div>
                                </form>
                            </div><!-- .End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .form-tab -->
                </div><!-- End .form-box -->
            </div>
        </div>
    </div>
    
</main><!-- End .main -->
@endsection
