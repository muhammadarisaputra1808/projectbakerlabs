<!DOCTYPE html>
<html lang="en">


<!-- molla/index-9.html  22 Nov 2019 09:57:25 GMT -->
<head>
     <!-- CSRF Token -->
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta http-equiv="X-UA-Compatible" content="ie=edge" />
     <title>BakerLabs</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('molla/assets/images/icons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('molla/assets/images/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('molla/assets/images/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('molla/assets/images/icons/site.html')}}">
    <link rel="mask-icon" href="{{asset('molla/assets/images/icons/safari-pinned-tab.svg')}}" color="#666666">
    <link rel="shortcut icon" href="{{asset('molla/assets/images/icons/favicon.ico')}}">
    {{-- <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="{{asset('molla/assets/images/icons/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff"> --}}
    <link rel="stylesheet" href="{{asset('molla/assets/vendor/line-awesome/line-awesome/line-awesome/css/line-awesome.min.css')}}">
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="{{asset('molla/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('molla/assets/css/plugins/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('molla/assets/css/plugins/magnific-popup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('molla/assets/css/plugins/jquery.countdown.css')}}">
    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{asset('molla/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('molla/assets/css/skins/skin-demo-9.css')}}">
    <link rel="stylesheet" href="{{asset('molla/assets/css/demos/demo-9.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.min.css')}}">
    {{-- <link href="https://cdn.jsdelivr.net/npm/sweetalert2@10" rel="stylesheet"> --}}
    <style>
        @media screen and (max-width: 991px) {
            .header-6 .header-middle .header-left {
                display:none
            }

            .header-6 .header-middle .header-center {
                display: block
            }

            .header-6 .logo {
                position: static !important;
                transform: translate(0,0) !important;
                margin-top: 0 !important;
                margin-bottom: 0 !important
            }
        }
        .table-order td, .table-order th{
            padding: 1.4rem;
        }
        .rating {
            direction: rtl;
            font-size: 24px;
        }

        .rating input {
            display: none;
        }

        .rating label {
            color: #ddd;
            cursor: pointer;
            font-size: 24px;
            transition: color 0.2s;
        }

        .rating input:checked ~ label,
        .rating input:hover ~ label,
        .rating label:hover ~ label {
            color: #ffcc00;
        }
    </style>
</head>
<body>
    <div class="page-wrapper">  
        @php
            $kategori = App\Models\Kategori::all();
        @endphp
        @include('layouts.user.header')
        
        @yield('content')

        @include('layouts.user.footer')
        
    </div><!-- End .page-wrapper -->
    <button id="scroll-top" title="Back to Top"><i class="icon-arrow-up"></i></button>

    @include('layouts.user.mobilemenu')
    
    @include('layouts.user.login')

    @include('sweetalert::alert')
    <!-- Plugins JS File -->
    <script src="{{asset('molla/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/jquery.hoverIntent.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/superfish.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/bootstrap-input-spinner.js')}}"></script>
    <script src="{{asset('molla/assets/js/jquery.plugin.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('molla/assets/js/jquery.countdown.min.js')}}"></script>
    <!-- Main JS File -->
    <script src="{{asset('molla/assets/js/main.js')}}"></script>
    <script src="{{asset('molla/assets/js/demos/demo-9.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @yield('script')
</body>


<!-- molla/index-9.html  22 Nov 2019 09:58:03 GMT -->
</html>