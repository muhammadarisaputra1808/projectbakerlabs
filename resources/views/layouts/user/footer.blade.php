<footer class="footer footer-2">
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                    <div class="widget widget-about">
                        <img src="{{asset('img/BAKERLABS-2.png')}}" class="footer-logo" alt="Footer Logo" width="300">
                        
                        <div class="widget-about-info">
                            <div class="row">
                                <div class="col-sm-6 col-md-6">
                                    <span class="widget-about-title">Metode Pembayaran</span>
                                    <figure class="footer-payments">
                                        <img src="{{ asset('img/metode-pembayaran.png') }}" alt="Payment methods" width="272" height="20">
                                    </figure><!-- End .footer-payments -->
                                </div><!-- End .col-sm-6 -->
                                <div class="col-sm-6 col-md-6">
                                    <span class="widget-about-title">Ada Pertanyaan? Hubungi Kami 24/7</span>
                                    <a href="tel:123456789">+628 99 7337 188</a>
                                </div><!-- End .col-sm-6 -->
                                
                            </div><!-- End .row -->
                        </div><!-- End .widget-about-info -->
                    </div><!-- End .widget about-widget -->
                </div><!-- End .col-sm-12 col-lg-3 -->

                <div class="col-sm-4 col-lg-2">
                    <div class="widget">
                        <h4 class="widget-title">Informasi</h4><!-- End .widget-title -->
                        <ul class="widget-list">
                            <li><a href="about.html">Tentang Bakerlabs</a></li>
                            <li><a href="faq.html">FAQ</a></li>
                            <li><a href="contact.html">Contact us</a></li>
                            <li><a href="login.html">Log in</a></li>
                        </ul><!-- End .widget-list -->
                    </div><!-- End .widget -->
                </div><!-- End .col-sm-4 col-lg-3 -->

                <div class="col-sm-4 col-lg-2">
                    <div class="widget">
                        <h4 class="widget-title">Customer Service</h4><!-- End .widget-title -->

                        <ul class="widget-list">
                            <li><a href="#">Metode Pembayaran</a></li>
                            <li><a href="#">Garansi !</a></li>
                            <li><a href="#">Pengiriman</a></li>
                            <li><a href="#">Syarat dan Ketentuan</a></li>
                            <li><a href="#">Kebijakan Pribadi</a></li>
                        </ul><!-- End .widget-list -->
                    </div><!-- End .widget -->
                </div><!-- End .col-sm-4 col-lg-3 -->

                <div class="col-sm-4 col-lg-2">
                    <div class="widget">
                        <h4 class="widget-title">My Account</h4><!-- End .widget-title -->

                        <ul class="widget-list">
                            <li><a href="#">Masuk/Daftar</a></li>
                            <li><a href="cart.html">Keranjang</a></li>
                            <li><a href="#">Bantuan</a></li>
                        </ul><!-- End .widget-list -->
                    </div><!-- End .widget -->
                </div><!-- End .col-sm-64 col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .footer-middle -->

    <div class="footer-bottom">
        <div class="container">
            <p class="footer-copyright">Copyright &copy; <span id="year"></span> Bakerlabs. Seluruh Hak Cipta.</p><!-- End .footer-copyright -->
            <script>
                document.getElementById("year").textContent = new Date().getFullYear();
            </script>
            <ul class="footer-menu">
                <li><a href="#">Syarat Penggunaan</a></li>
                <li><a href="#">Kebijakan Pribadi</a></li>
            </ul><!-- End .footer-menu -->

            <div class="social-icons social-icons-color">
                <span class="social-label">Sosial Media</span>
                <a href="#" class="social-icon social-instagram" title="Instagram" target="_blank"><i class="icon-instagram"> BakerLabs</i></a>
            </div><!-- End .soial-icons -->
        </div><!-- End .container -->
    </div><!-- End .footer-bottom -->
</footer><!-- End .footer -->