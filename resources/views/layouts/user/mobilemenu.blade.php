<!-- Mobile Menu -->
<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

<div class="mobile-menu-container">
    <div class="mobile-menu-wrapper">
        <span class="mobile-menu-close"><i class="icon-close"></i></span>

        <form action="#" method="get" class="mobile-search">
            <label for="mobile-search" class="sr-only">Search</label>
            <input type="search" class="form-control" name="mobile-search" id="mobile-search" placeholder="Cari Produk ..." required>
            <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
        </form>
        
        <nav class="mobile-nav">
            <ul class="mobile-menu">
                <li>
                    <a href="{{ url('/') }}" class="">Home</a>
                </li>
                <li>
                    <a href="{{ url('/menu') }}" class="">Produk</a>
                </li>
                <li>
                    <a href="#" class="sf-with-ul">Kategori</a>
                    <ul>
                        @foreach ($kategori as $item)
                        <div class="menu-title">
                            <li><a href="{{ url('/menu/'.$item->slug ) }}">{{ $item->nama }}</a></li>
                        </div>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="{{ url('/tentang-kami') }}" class="">Tentang Kami</a>
                </li>
                <li>
                    @auth
                    @if (auth()->user()->is_seller)
                    <a href="{{ url('/toko') }}">Toko Saya</a>
                    @else
                    <a href="{{ url('/daftartoko') }}">Toko Saya</a>
                    @endif
                    @endauth
                </li>
            </ul>
        </nav><!-- End .mobile-nav -->

        <div class="social-icons">
            <a href="#" class="social-icon" target="_blank" title="Facebook"><i class="icon-facebook-f"></i></a>
            <a href="#" class="social-icon" target="_blank" title="Twitter"><i class="icon-twitter"></i></a>
            <a href="#" class="social-icon" target="_blank" title="Instagram"><i class="icon-instagram"></i></a>
            <a href="#" class="social-icon" target="_blank" title="Youtube"><i class="icon-youtube"></i></a>
        </div><!-- End .social-icons -->
    </div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->