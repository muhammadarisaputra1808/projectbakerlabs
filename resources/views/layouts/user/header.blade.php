<header class="header header-6">
    <div class="header-top shadow">
        <div class="container py-3">
            <div class="header-left">
                <ul class="top-menu top-link-menu d-none d-md-block">
                    <li>
                        <a href="#">Links</a>
                        <ul>
                            <li><a href="https://wa.me/+6287742890592?" target="_blank"><i class="icon-phone"></i>Call: +62 877 4289 0592</a></li>
                        </ul>
                    </li>
                </ul><!-- End .top-menu -->
            </div><!-- End .header-left -->
            
            <div class="header-right">
                <div class="social-icons social-icons-color mr-2">
                    <a href="#" class="social-icon social-instagram" title="Pinterest" target="_blank"><i class="icon-instagram"> BakerLabs</i></a>
                </div><!-- End .soial-icons -->
                @if (auth()->check())
                <ul class="top-menu">
                    <li>
                        <a href="#"><i class="icon-user"></i>Akun Saya</a>
                        <ul>
                            <li><a href="{{ route('profile.view') }}"><i class="icon-user"></i>Dashboard</a></li>
                            <li class="d-xl-none d--block">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i
                                        class="icon-long-arrow-right"></i>  Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>                
                            </li>
                        </ul>
                    </li>
                </ul>
                @else
                <a href="{{ route('login') }}">  <i class="fa-solid fa-user"></i> Masuk/Daftar</a>
                @endif
            </div>
        </div>
    </div>

    {{-- HEADER BAWAH --}}
    <div class="sticky-wrapper">
        <div class="header-bottom sticky-header">
            <div class="container">
                <div class="header-left">
                    <button class="mobile-menu-toggler mr-4">
                        <span class="sr-only">Toggle mobile menu</span>
                        <i class="icon-bars"></i>
                    </button>
                    <a href="/">
                        <img src="{{asset('img/BAKERLABS.png')}}" alt="BAKERLABS LOGO" width="150" height="auto">
                    </a>
                    <nav class="main-nav">
                        <ul class="menu sf-arrows">
                            <li>
                                <a href="{{ url('/') }}" class="">Home</a>
                            </li>
                            <li>
                                <a href="{{ url('/menu') }}" class="">Produk</a>
                            </li>
                            <li>
                                <a href="#" class="sf-with-ul">Kategori</a>
                                <ul>
                                    @foreach ($kategori as $item)
                                    <div class="menu-title">
                                        <li><a href="{{ url('/menu/'.$item->slug ) }}">{{ $item->nama }}</a></li>
                                    </div>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/tentang-kami') }}" class="">About Us</a>
                            </li>
                            <li>
                                @auth
                                @if (auth()->user()->is_seller)
                                <a href="{{ url('/toko') }}"><i class="fa-solid fa-store"></i> Toko </a>
                                @else
                                <a href="{{ url('/daftartoko') }}"><i class="fa-solid fa-store"></i> Daftar Toko</a>
                                @endif
                                @endauth
                            </li>
                        </ul><!-- End .menu -->
                    </nav><!-- End .main-nav -->
                </div><!-- End .header-left -->
                <div class="header-right">
                   
                </div>
                <div class="header-left">
                    <div class="dropdown cart-dropdown mr-3">
                        <a href="/cart" class="dropdown-toggle " aria-haspopup="true" aria-expanded="false" data-display="static">
                            <i class="icon-shopping-cart"></i>
                            <span class="cart-count">
                                {{ \App\Models\Cart::where('user_id', auth()->id())->count() }}
                            </span>
                        </a>
                    </div><!-- End .cart-dropdown -->
                    <div class="header-search header-search-extended header-search-visible mr-0 shadow-sm px-2 bg-body rounded">
                        <form action="{{ route('produks.search') }}" method="get">
                            <div class="header-search-wrapper search-wrapper-wide">
                                <label for="q" class="sr-only">Search</label>
                                <button class="btn btn-primary" type="submit"><i class="icon-search"></i></button>
                                <input type="search" class="form-control" id="q" name="q" value="{{ request()->q }}" placeholder="Search product ..."
                                required>
                            </div><!-- End .header-search-wrapper -->
                        </form>
                    </div><!-- End .header-search -->
                </div>
            </div><!-- End .container -->
        </div><!-- End .header-bottom -->
    </div>

</header><!-- End .header -->