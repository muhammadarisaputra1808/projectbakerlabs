<!doctype html>
<html lang="en">

<head>
    <!-- CSRF Token -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Toko Saya | BakerLabs</title>
    <!-- CSS files -->
    <link href="{{ asset('tabler/css/tabler.min.css?1674944402') }}" rel="stylesheet" />
    <link href="{{ asset('tabler/css/tabler-flags.min.css?1674944402') }}" rel="stylesheet" />
    <link href="{{ asset('tabler/css/tabler-payments.min.css?1674944402') }}" rel="stylesheet" />
    <link href="{{ asset('tabler/css/tabler-vendors.min.css?1674944402') }}" rel="stylesheet" />
    <link href="{{ asset('tabler/css/demo.min.css?1674944402') }}" rel="stylesheet" />
    
    <style>
        @import url('https://rsms.me/inter/inter.css');

        :root {
            --tblr-font-sans-serif: 'Inter Var', -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
        }

        body {
            font-feature-settings: "cv03", "cv04", "cv11";
        }
    </style>
</head>

<body>
    <script src="{{ asset('tabler/js/demo-theme.min.js?1684106062') }}"></script>
    <div id="app" class="page">
        
        @include('layouts.seller.sidebar')
        @include('layouts.seller.navbar')
        <main class="page-wrapper">
            @yield('content')
        </main>
    </div>
    @include('sweetalert::alert')
    <!-- Libs JS -->
    <script src="{{ asset('tabler/libs/fslightbox/index.js?1695847769') }}" defer=""></script>
    <script src="{{ asset('tabler/libs/apexcharts/dist/apexcharts.min.js?1674944402') }}" defer></script>
    <script src="{{ asset('tabler/libs/jsvectormap/dist/js/jsvectormap.min.js?1674944402') }}" defer></script>
    <script src="{{ asset('tabler/libs/jsvectormap/dist/maps/world.js?1674944402') }}" defer></script>
    <script src="{{ asset('tabler/libs/jsvectormap/dist/maps/world-merc.js?1674944402') }}" defer></script>
    <!-- Tabler Core -->
    <script src="{{ asset('tabler/js/tabler.min.js?1674944402') }}" defer></script>
    <script src="{{ asset('tabler/js/demo.min.js?1674944402') }}" defer></script>
</body>

</html>