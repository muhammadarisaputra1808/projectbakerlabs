@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Data Penarikan Saldo
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Penarikan Saldo</h3>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table table-vcenter card-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No .</th>
                                            <th>Nama Toko</th>
                                            <th>No. Telepon</th>
                                            <th>Nomor Rekening</th>
                                            <th>Nama Pemilik</th>
                                            <th>Bank</th>
                                            <th>Jumlah Penarikan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($requestSaldo) == 0)
                                        <tr class="text-center">
                                            <td colspan="8">Belum Ada Data Yang Ditambahkan</td>
                                        </tr>
                                        @else
                                        @foreach ($requestSaldo as $request)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $request->toko->nama }}</td>
                                                <td>{{ $request->toko->no_telepon }}</td>
                                                <td>{{ $request->toko->nomor_rekening ? $request->toko->nomor_rekening : '-'}}</td>
                                                <td>{{ $request->toko->nama_pemilik ? $request->toko->nama_pemilik : '-' }}</td>
                                                <td>{{ $request->toko->bank ? $request->toko->bank : '-' }}</td>
                                                <td>{{ $request->jumlah }}</td>
                                                @if ($request->keterangan == 'Request Penarikan Saldo Diterima')
                                                    <td>
                                                        <button class="btn btn-primary">Accepted</button>
                                                    </td>
                                                @else
                                                <td>
                                                    <a href="{{ route('accept.request', $request->id) }}" class="btn btn-primary">Terima Request</a>
                                                </td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection