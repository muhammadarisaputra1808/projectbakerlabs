@extends('layouts.seller.main')
@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <h2 class="page-title">
                    Data Toko
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="page-body">
    <div class="container-xl">
        <div class="row row-cards">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Toko</h3>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table table-vcenter card-table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No .</th>
                                            <th>Nama Toko</th>
                                            <th>No. Telepon</th>
                                            <th>Alamat</th>
                                            <th>Nomor Rekening</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($toko) == 0)
                                        <tr class="text-center">
                                            <td colspan="8">Belum Ada Produk Yang Ditambahkan</td>
                                        </tr>
                                        @endif
                                        @foreach ($toko as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->nama }}</td>
                                                <td>{{ $item->no_telepon }}</td>
                                                <td>{{ $item->alamat . ', ' .$item->getKecamatanFullAttributes() . ', ' . $item->kode_pos}}</td>
                                                <td>{{ $item->nomor_rekening ? $item->nomor_rekening : '-'}}</td>
                                                <td> Rp. {{number_format($item->getSaldoTotal(), 0, ',', '.') }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection