<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Cart;

class SesiController extends Controller
{
    public function index(Request $request){
        $kategori = Kategori::all();
        $produks = Produk::with('kategori')->with('images')->get();
        // dd($produks);
        // dd($produks->first()->images->first()->images);

        return view('customer.index', compact('produks', 'kategori'));
    }

    function login(Request $request){
        $kategori = Kategori::all();
        $produk = Produk::with('kategori')->get();
        return view('customer.login', compact('produk', 'kategori'));
    }
    

    function proseslogin(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required'=>'Email Wajib Diisi !',
            'password.required'=>'Password Wajib Diisi !'
        ]);
    
        $credentials = $request->only('email', 'password');
    
        if(Auth::attempt($credentials)){
            return redirect('/');
        } else {
            return redirect('/login')->withErrors('Email dan Password Tidak Sesuai !')->withInput();
        }
    }
    

    function logout()
    {
        Auth::logout();
        return redirect('');
    }
}
