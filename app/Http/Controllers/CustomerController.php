<?php

namespace App\Http\Controllers;

use App\Models\ProdukImage;
use App\Models\toko;
use App\Models\User;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\Review;
use Illuminate\Http\Request;
use Carbon\Carbon;



class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $kategori = Kategori::all();
        $produks = Produk::with('kategori', 'reviews')->paginate(8);
        // dd($produks->first());
        return view('customer.index', compact('produks', 'kategori'));
    }
    public function loadMore(Request $request)
    {
        $skip = $request->skip;
        $take = 12; // Jumlah produk yang akan dimuat setiap kali "Load More" diklik

        $produks = Produk::with('images', 'kategori', 'reviews')->skip($skip)->take($take)->get();
        return response()->json($produks);
    }
    public function search(Request $request)
    {
        $query = Produk::query();

        if ($request->has('q') && $request->q) {
            $query->where('nama', 'like', '%' . $request->q . '%');
        }

        if ($request->has('min_price') && $request->min_price) {
            $query->where('harga_netto', '>=', $request->min_price);
        }

        if ($request->has('max_price') && $request->max_price) {
            $query->where('harga_netto', '<=', $request->max_price);
        }
        
        if ($request->has('sort_by') && $request->sort_by) {
            if ($request->sort_by == 'harga_terendah') {
                $query->orderBy('harga_netto', 'asc');
            } elseif ($request->sort_by == 'harga_tertinggi') {
                $query->orderBy('harga_netto', 'desc');
            } elseif ($request->sort_by == 'terbaru') {
                $query->orderBy('created_at', 'desc');
            }
        }

        $produks = $query->paginate(5);
        $kategori = Kategori::all();
        return view('customer.menu.index', compact('produks', 'kategori'));
    }
    public function showMenu()
    {
        $kategori = Kategori::all();
        $produks = Produk::with('kategori')->with('images')->paginate(5);
        return view('customer.menu.index', compact('produks', 'kategori'));
    }

    public function showKategori($slug)
    {
        $kategoris = Kategori::all();
        $kategori = Kategori::where('slug', '=', $slug)->first();

        // dd($slug);
        if($kategori){
            $produks = Produk::where('kategori_id', '=', $kategori->id)->with('kategori')->with('images')->paginate(5);
        }else{
            $produks = Produk::with('kategori')->with('images')->paginate(5);
        }
        $imageProduk = Produk::with('images')->get();
        return view('customer.menu.produk', compact('produks', 'kategoris', 'kategori', 'imageProduk'));
    }

    public function sortBy(Request $request, $slug){
        $kategoris = Kategori::all();
        $kategori = Kategori::where('slug', '=', $slug)->first();
        $query = Produk::query();
        if ($request->has('min_price') && $request->min_price) {
            $query->where('kategori_id', $kategori->id)->where('harga_netto', '>=', $request->min_price);
        }

        if ($request->has('max_price') && $request->max_price) {
            $query->where('kategori_id', $kategori->id)->where('harga_netto', '<=', $request->max_price);
        }

        if ($request->has('sort_by') && $request->sort_by) {
            if ($request->sort_by == 'harga_terendah') {
                $query->where('kategori_id', $kategori->id)->orderBy('harga_netto', 'asc');
            } elseif ($request->sort_by == 'harga_tertinggi') {
                $query->where('kategori_id', $kategori->id)->orderBy('harga_netto', 'desc');
            } elseif ($request->sort_by == 'terbaru') {
                $query->where('kategori_id', $kategori->id)->orderBy('created_at', 'desc');
            }
        }

        $produks = $query->paginate(5);
        return view('customer.menu.produk', compact('produks', 'kategoris', 'kategori'));
    }
    public function showKategoriProduk($slug)
    {
        $kategoris = Kategori::all();
        $kategori = Kategori::where('slug', '=', $slug)->first();

        // dd($slug);
        if($kategori){
            $produks = Produk::where('kategori_id', '=', $kategori->id)->with('kategori')->with('images')->get();
        }else{
            $produks = Produk::with('kategori')->with('images')->get();
        }
        return view('customer.menu.produk', compact('produks', 'kategoris', 'kategori'));
    }

    function showProdukDetail($slug)
    {
        $produk = Produk::with('kategori', 'images')->where('slug', $slug)->first();
        //dd($produk);
        $kategori = Kategori::all();
        //dd($produks);
        $toko = Toko::where('user_id', $produk->user_id)->first();
        $imageProduk = Produk::with('images')->get();
        //dd($toko);
        //dd($imageProduk);
        $reviews = Review::where('produk_id', $produk->id)->get();
        // dd($reviews);
        return view('customer.produk', compact('produk', 'imageProduk', 'kategori', 'toko'));
    }

    function tentangkami()
    {
        return view('customer.tentangkami');
    }
}
