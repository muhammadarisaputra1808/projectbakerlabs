<?php

namespace App\Http\Controllers;

use App\Models\toko;
use App\Models\Order;
use App\Models\Produk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\OrderProcessed;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Notification;

class PesananController extends Controller
{
    public $biayaLayanan = 2000;
    public $biayaPengiriman = 7000;
    public function index(){
        $toko = Toko::with('user')->where('user_id', '=', auth()->user()->id)->first();
        $pesanans = Order::whereNot('status', 'Menunggu')->where('toko_id', $toko->id )->with('user','produk')->get();
        // dd($pesanans);
        $pesanan = $pesanans->groupBy('no_pesanan')->map(function ($group) {
            return $group->first();
        });
        $biayaPengiriman = $this->biayaPengiriman;
        return view('seller.pesanan.index', compact('pesanan', 'toko', 'biayaPengiriman'));
    }
    public function detailPesanan($no_pesanan){
        // $detailPesanan = Order::where('no_pesanan')
        $toko = Toko::with('user')->where('user_id', '=', auth()->user()->id)->first();
        $detailPesanan = Order::where('no_pesanan', $no_pesanan)->where('toko_id', $toko->id)->with('produk', 'user')->get();
        // dd($detailPesanan);
        $produk = [];
        foreach($detailPesanan as $item){
            if($item->produk->user_id == $toko->id){
                $produk[] = $item->produk->nama;
            }
        }
        // dd($produk);
        return view('seller.pesanan.detail', compact('detailPesanan', 'toko', 'produk'));
    }

    public function prosesPesanan($no_pesanan){
        $pesanan = Order::where('no_pesanan', $no_pesanan)->with('user.profile')->get();
        // dd($pesanan);
        foreach($pesanan as $item){
            $item->status = 'Dikirim';
            $item->save();
        }
        Notification::route('mail', 'bakerlabs943@gmail.com')->notify(new OrderProcessed($pesanan->first()));
        // Notification::route('mail', 'brianprasetyo01@gmail.com')->notify(new OrderProcessed($pesanan->first()));
        Alert::success('Hore!', 'Pesanan Berhasil Di Proses Dan Paket Akan Di Jemput Oleh Kurir');
        return redirect('toko/pesanan');
    }
}
