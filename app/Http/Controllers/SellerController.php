<?php

namespace App\Http\Controllers;

use App\Models\toko;
use App\Models\User;
use App\Models\Order;
use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class SellerController extends Controller
{
    public function getToko()
    {
        return toko::with('user')->where('user_id', '=', auth()->user()->id)->first();
    }
    public function index()
    {
        $toko = $this->getToko();
        $jumlahPenjualan = Order::where('toko_id', $toko->id)->where('status', 'Diterima')->count();
        // dd($jumlahPenjualan);
        return view('seller.index', compact('toko', 'jumlahPenjualan'));
    }

    public function editToko()
    {
        $toko = $this->getToko();
        $kecamatan = [
            'Alang-alang Lebar', 'Bukit Kecil', '	Gandus', 'Ilir Barat I', 'Ilir Barat II', 'Ilir Timur I', 'Ilir Timur II', 'Ilir Timur III', 'Jakabaring', 'Kalidoni',
            'Kemuning', 'Kertapati', 'Plaju', 'Sako', 'Seberang Ulu I', 'Seberang Ulu II', 'Sematang Borang', 'Sukarami'
        ];
        return view('seller.update', compact('toko', 'kecamatan'));
    }
    public function updateToko(Request $request)
    {
        $toko = $this->getToko();
        $request->validate([
            'nama' => 'required|string|min:3|max:255|unique:tokos,nama,' . $toko->id,
            'no_telepon' => 'required|string|min:10|max:255',
            'kecamatan' => 'required|string',
            'alamat' => 'required|string|min:3|max:255',
            'kode_pos' => 'required|string|min:3|max:255',
        ], [
            'nama.required' => 'Kolom wajib di isi *',
            'nama.min' => 'Minimal 3 karakter',
            'nama.unique' => 'Nama Sudah Digunakan',
            'no_telepon.required' => 'Kolom wajib di isi *',
            'no_telepon.min' => 'Minimal 10 karakter',
            'alamat.required' => 'Kolom wajib di isi *',
            'kode_pos.required' => 'Kolom wajib di isi *',
        ]);
        // dd($request->all());

        $toko->nama = $request->nama;
        $toko->no_telepon = $request->no_telepon;
        $toko->kecamatan = $request->kecamatan . ', Palembang, Sumatera Selatan.';
        $toko->alamat = $request->alamat;
        $toko->kode_pos = $request->kode_pos;
        $toko->save();
        Alert::success('Hore!', 'Profil Toko Berhasil Diperbarui!');
        return redirect('toko');
    }
    public function updatePhoto(Request $request, $profile_id)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048'
        ], [
            'image.required' => 'Pilih Gambar Terlebih dahulu **',
        ]);
        if ($request->has('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $path = 'uploads/toko/';
            $file->move($path, $filename);
        } else {
            $path = 'img/';
            $filename = 'default.jpg';
        }
        Toko::findOrFail($profile_id)->update([
            'image' => $path . $filename
        ]);
        Alert::success('Hore!', 'Berhasil Memperbarui Gambar');
        return back();
    }
    public function createtoko()
    {
        $kecamatan = [
            'Alang-alang Lebar', 'Bukit Kecil', '	Gandus', 'Ilir Barat I', 'Ilir Barat II', 'Ilir Timur I', 'Ilir Timur II', 'Ilir Timur III', 'Jakabaring', 'Kalidoni',
            'Kemuning', 'Kertapati', 'Plaju', 'Sako', 'Seberang Ulu I', 'Seberang Ulu II', 'Sematang Borang', 'Sukarami'
        ];
        return view('seller.createtoko', compact('kecamatan'));
    }

    public function storetoko(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|min:3|max:255|unique:tokos,nama',
            'no_telepon' => 'required|string|min:10|max:255',
            'kecamatan' => 'required|string',
            'alamat' => 'required|string|min:3|max:255',
            'kode_pos' => 'required|string|min:3|max:255',
            'image' => 'nullable|image|mimes:png,jpg,jpeg,webp',
        ], [
            'nama.required' => 'Kolom wajib di isi *',
            'nama.min' => 'Minimal 3 karakter',
            'nama.unique' => 'Nama Sudah Digunakan',
            'no_telepon.required' => 'Kolom wajib di isi *',
            'no_telepon.min' => 'Minimal 10 karakter',
            'kecamatan.required' => 'Kolom wajib di isi *',
        ]);
        if ($request->has('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $path = 'uploads/toko/';
            $file->move($path, $filename);
        } else {
            $path = 'img/';
            $filename = 'default.jpg';
        }
        $user_id = $request->user()->id;
        User::where('id', $user_id)->update(['is_seller' => '1']);
        toko::create([
            'nama' => $request->nama,
            'user_id' => $user_id,
            'no_telepon' => $request->no_telepon,
            'kecamatan' => $request->kecamatan . ', Palembang, Sumatera Selatan.',
            'alamat' => $request->alamat,
            'kode_pos' => $request->kode_pos,
            'image' => $path . $filename
        ]);
        Alert::success('Hore!', 'Berhasil Membuat Toko');
        return redirect('/');
    }

    public function showSaldo()
    {
        $toko = $this->getToko();
        $saldo = $toko->getSaldoTotal();
        // dd($saldo);
        return view('seller.saldo.index', compact('toko', 'saldo'));
    }
    public function updateNomorRekening(Request $request, toko $toko_id)
    {
        $request->validate([
            'bank' => 'required|string|min:3|max:255',
            'nomor_rekening' => 'required|string|min:3|max:255',
            'nama_pemilik' => 'required|string|min:3|max:255',

        ], [
            'bank.required' => 'Kolom wajib di isi *',
            'bank.min' => 'Minimal 3 karakter',
            'nomor_rekening.required' => 'Kolom wajib di isi *',
            'nomor_rekening.min' => 'Minimal 3 karakter',
            'nama_pemilik.required' => 'Kolom wajib di isi *',
            'nama_pemilik.min' => 'Minimal 3 karakter',
        ]);
        $toko_id->nomor_rekening = $request->nomor_rekening;
        $toko_id->nama_pemilik = $request->nama_pemilik;
        $toko_id->bank = $request->bank;
        $toko_id->save();
        Alert::success('Hore!', 'Data Berhasil Disimpan !');
        return redirect()->back();        
    }
    public function catatPenarikan(Request $request, $id)
    {
        $toko = Toko::findOrFail($id);
        $jumlah = $request->input('jumlah');
        $keterangan = "Request Penarikan Saldo";
        $tanggal = now();
        // dd($jumlah);
        if ($toko->catatPenarikan($jumlah, $keterangan, $tanggal)) {
            Alert::success('Hore!', 'Data Penarikan Saldo Berhasil Di Kirim !');
            return back();
        } else {
            Alert::warning('Warning!', 'Saldo Tidak Mencukupi !');
            return back();
        }
    }
}
