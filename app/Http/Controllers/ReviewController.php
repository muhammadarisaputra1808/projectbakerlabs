<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;

class ReviewController extends Controller
{
    public function store(Request $request, Produk $produk)
    {
        // dd($request->all());
        $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'review' => 'required|string',
        ]);
        Review::create([
            'produk_id' => $produk->id,
            'user_id' => auth()->id(),
            'rating' => $request->rating,
            'review' => $request->review,
        ]);
        Alert::success('Hore!', 'Ulasan Produk Berhasil Disimpan');
        return redirect()->back();
    }
    public function update(Request $request, Review $review)
    {
        // dd($review);

    $request->validate([
        'rating' => 'required|integer|min:1|max:5',
        'review' => 'required|string|max:1000',
    ]);

    $review->update([
        'rating' => $request->rating,
        'review' => $request->review,
    ]);
    Alert::success('Hore!', 'Ulasan Produk Berhasil Diperbarui');
    return redirect()->back();
}
}
