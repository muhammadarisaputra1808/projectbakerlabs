<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Cart;
use App\Models\toko;
use App\Models\User;
use App\Models\Order;
use App\Models\Produk;
use App\Models\Review;
use App\Models\Profile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\OrderPlaced;
use App\Notifications\OrderReceived;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Notification;

class CartController extends Controller
{
    public $biayaLayanan = 2000;
    public $biayaPenanganan = 2000;
    public $diskonPengiriman = 5000;
    public $biayaPengiriman = 7000;
    public $biayaProteksi = 2000;
    public function __construct()
    {
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = config('midtrans.serverKey');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = config('midtrans.isProduction');
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = config('midtrans.isSanitized');
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = config('midtrans.isSanitized');
    }
    public function index()
    {
        $user = Auth::user();
        $user_id = $user->id;
        // Order::where('user_id', $user_id)->where('status', 'Menunggu')->delete();
        $carts = Cart::with('produk.toko')->where('user_id', $user_id)->get();
        // dd();
        $total_subtotal = $carts->sum('total');
        $alamat = User::with('profile')->where('id', '=', Auth::user()->id)->first();
        $ordersPending = Order::where('user_id', $user_id)->where('status', 'Belum Bayar')->exists();
        // dd($alamat->profile == null);
        // dd($carts->isEmpty());

        return view('customer.cart.index', compact('carts', 'user', 'total_subtotal', 'alamat', 'ordersPending'));
    }
    public function cancelOrder($nomorOrder)
    {
        $order = Order::where('no_pesanan', $nomorOrder)->get();
        // dd($order);
        if($order->first()->status == 'Menunggu'){
            foreach($order as $item){
                $item->delete();
            }
        }
        return redirect('akun');
    }

    public function addToCart(Request $request)
    {
        $user_id = Auth::id();
        $produk_id = $request->input('produk_id');
        // dd($request->input('qty'));
        $qty = $request->input('qty');

        $produk = Produk::find($produk_id);
        if (!$produk) {
            return redirect()->back()->with('error', 'Produk tidak ditemukan');
        }

        $total = $produk->harga_netto * $qty;

        $existingCart = Cart::where('user_id', $user_id)->where('produk_id', $produk_id)->first();

        if ($existingCart) {
            $existingCart->qty += $qty;
            $existingCart->total += $total;
            $existingCart->save();
        } else {

            Cart::create([
                'user_id' => $user_id,
                'produk_id' => $produk_id,
                'no_pesanan' => time() . rand(1, 100),
                'qty' => $qty,
                'total' => $total,
            ]);
        }
        Alert::success('Hore!', 'Produk Berhasil Ditambahkan Ke Keranjang');
        return redirect()->back();
    }

    public function updateCart(Request $request, $user_id)
    {

        $cartItem = Cart::where('user_id', '=', $user_id)->where('produk_id', $request->produk_id)->first();
        $qty = $request->input('qty');
        $produk = Produk::find($request->produk_id);
        if (!$produk) {
            return redirect()->back()->with('error', 'Produk tidak ditemukan');
        }
        $total = $produk->harga_netto * $qty;
        // dd($cartItem);
        if ($cartItem) {
            $cartItem->qty = $qty;
            $cartItem->total = $total;
            $cartItem->save();
        }
        Alert::success('Hore!', 'Keranjang Berhasil Diupdate');
        return redirect()->back();
    }


    public function removeFromCart($cart_id)
    {
        // dd($cart_id);
        $cartItem = Cart::findOrFail($cart_id);
        if ($cartItem) {
            $cartItem->delete();
            // Opsional, Anda dapat menambahkan pesan keberhasilan di sini
        }
        Alert::success('Success!', 'Data Berhasil di Hapus');
        return back();
    }
    public function getTotalBelanja($nomorOrder)
    {
        $user = Auth()->user();
        $orders =  Order::with('produk', 'toko')->where('user_id', $user->id)->where('no_pesanan', $nomorOrder)->get();
        $ordersByStore = $orders->groupBy('toko.nama');
        $totalSubtotal = $this->biayaLayanan;
        foreach ($ordersByStore as $storeName => $orders) {
            $totalSubtotal += $orders->first()->sub_total;
        }
        // $toko = Toko::whereHas('orders', function ($query) use ($user) {
        //     $query->where('user_id', $user->id);
        // })->with(['orders' => function ($query) use ($user) {
        //     $query->where('user_id', $user->id)->with('produk');
        // }])->get();

        // $totalSubtotal = 0;
        // foreach ($toko as $item) {
        //     $totalSubtotal += $item->orders->first()->sub_total;
        // }

        // $totalBelanja = $totalSubtotal + $this->biayaLayanan + $this->biayaPenanganan - $this->diskonPengiriman;
        return $totalSubtotal;
    }
    public function getToko()
    {
        $user = Auth()->user();
        $toko = Toko::whereHas('orders', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->with(['orders' => function ($query) use ($user) {
            $query->where('user_id', $user->id)->with('produk');
        }])->get();

        return $toko;
    }
    public function checkout(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->id;

        $nomorOrder = $this->generateNomorOrder();
        $alamat = User::with('profile')->where('id', '=', $user_id)->first();
        $carts = Cart::with('produk.toko')->where('user_id', $user_id)->get();

        // Mengelompokkan produk berdasarkan toko
        $tokoGrouped = $carts->groupBy(function ($cart) {
            return $cart->produk->toko->id;
        });

        // Menghitung subtotal per toko
        $subtotals = $tokoGrouped->map(function ($group, $tokoId) {
            $subtotal = $group->sum(function ($cart) {
                return $cart->produk->harga_netto * $cart->qty;
            });

            return [
                'toko_id' => $tokoId,
                'toko_nama' => $group->first()->produk->toko->nama,
                'subtotal' => $subtotal
            ];
        });

        foreach ($carts as $cart) {
            $subtotalData = $subtotals->firstWhere('toko_id', $cart->produk->toko->id);
            // dd($subtotalData);
            $order = Order::updateOrCreate([
                'produk_id' => $cart->produk_id,
                'user_id' => $user_id,
                'status' => 'Menunggu',
            ], [
                'no_pesanan' => $nomorOrder,
                'toko_id' => $cart->produk->toko->id,
                'qty' => $cart->qty,
                'total' => $cart->total,
                'sub_total' => $subtotalData['subtotal'],
                'tgl_pesanan' => now(),
            ]);
        }
        $orders = Order::with('produk', 'toko')->where('user_id', $user_id)->where('no_pesanan', $nomorOrder)->get();
        $ordersByStore = $orders->groupBy('toko.nama');
        $totalSubtotal = $this->biayaLayanan;
        $items = []; // Inisialisasi array items
        foreach ($ordersByStore as $storeName => $order) {
            // $totalSubtotal += $this->biayaPenanganan;
            $totalSubtotal += $order->first()->sub_total + $this->biayaPengiriman;
            foreach ($order as $singleOrder) {
                $items[] = array(
                    'id'       => $singleOrder->produk->id,
                    'price'    => $singleOrder->produk->harga_netto,
                    'quantity' => $singleOrder->qty,
                    'name'     => $singleOrder->produk->nama
                );
            }
            // foreach($order){
            //     $items = array(
            //         array(
            //             'id'       => 'item1',
            //             'price'    => 100000,
            //             'quantity' => 1,
            //             'name'     => 'Adidas f50'
            //         ),
            //     );
            // }
        }
        // dd($totalSubtotal);
        if (!$orders->isEmpty()) {
            
            foreach ($orders as $order) {
                $shipping_address = array(
                    'first_name'   => $user->profile->nama,
                    'address'      => $user->profile->alamat,
                    'city'         => $user->profile->kecamatan,
                    'postal_code'  => $user->profile->kode_pos,
                );
                $params = array(
                    'transaction_details' => array(
                        'order_id' => $order->no_pesanan,
                        'gross_amount' => $totalSubtotal,
                    ),
                    'customer_details' => array(
                        'first_name' => $user->profile->nama,
                        'email' => $user->email,
                        'phone'            => $user->profile->no_telepon,
                        'shipping_address' => $shipping_address
                    ),
                    // 'item_details' => $items
                );
                // dd($params);

                $snapToken = \Midtrans\Snap::getSnapToken($params);
                $order->update([
                    'snap_token' => $snapToken
                ]);
            }
            // dd($orders);

            $user = Auth()->user();
            // Cart::where('user_id', $user->id)->delete();
            $expiresAt = $orders->first()->created_at->addMinutes(10);
            $totalBelanja = $totalSubtotal;
            // dd($totalBelanja);
            $biayaLayanan = $this->biayaLayanan;
            $biayaPenanganan = $this->biayaPenanganan;
            $diskonPengiriman = $this->diskonPengiriman;
            $biayaPengiriman = $this->biayaPengiriman;
            $biayaProteksi = $this->biayaProteksi;

            // dd($orders);
            // dd($toko);

        } else {
            return view('404');
        }
        return view('customer.cart.order', compact('expiresAt','orders', 'ordersByStore', 'user', 'alamat',  'biayaPengiriman', 'biayaProteksi', 'biayaLayanan', 'biayaPenanganan', 'diskonPengiriman', 'totalBelanja'));
    }
    public function deleteOrder($no_pesanan)
    {
        $orders = Order::where('no_pesanan', $no_pesanan)->get();
        // dd($orders);
        foreach($orders as $order){
            $order->delete();
        }
        return response()->json(['message' => 'Order deleted successfully'], 200);
    }

    public function finishOrder($no_pesanan){
        $orders = Order::where('no_pesanan', $no_pesanan)->with('produk', 'toko')->get();
        $toko_id = $orders->groupBy('toko.id');
        foreach($toko_id as $toko => $order){
            $toko = Toko::findOrFail($toko);
            $jumlah = $order->first()->sub_total;
            $keterangan = 'Order masuk dengan nomor pesanan : '. $order->first()->no_pesanan;
            // dd($keterangan);

            $toko->tambahSaldo($jumlah, $keterangan);
        }
        foreach($orders as $order){
            $order->status = 'Diterima';
            $order->save();
            $produk = Produk::where('id', $order->produk->id)->first();
            // dd($produk);
            $produk->stok -= $order->qty;
            $produk->save();
        }
        Alert::success('Pesanan Diterima', 'Terima Kasih Telah Berbelanja Di BakerLabs');
        return redirect('akun');
        // dd($orders);
    }
    public function success($no_pesanan)
    {
        $user = Auth()->user();
        Cart::where('user_id', $user->id)->delete();
        $orders = Order::where('no_pesanan', $no_pesanan)->get();
        Notification::route('mail', $user->email)->notify(new OrderPlaced($orders->first(), $this->getTotalBelanja($no_pesanan), $user->name));
        // dd($order);
        $toko_ids = $orders->pluck('toko_id')->unique(); // Mengambil semua toko_id dan menghapus duplikat
        foreach ($toko_ids as $toko_id) {
            $toko = Toko::find($toko_id); // Asumsikan ada model Toko
            Notification::route('mail', $toko->user->email)->notify(new OrderReceived($orders->where('toko_id', $toko_id), $this->getTotalBelanja($no_pesanan), $toko->name));
        }
        foreach ($orders as $item) {
            $item->status = 'Dikemas';
            $item->save();
        }
        return redirect()->route('pesanan.show.details', $orders->first()->no_pesanan);
    }
    public function detailPesanan($no_pesanan)
    {
        $user = auth()->user();
        $user_id = $user->id;
        $alamat = User::with('profile')->where('id', '=', $user_id)->first();
        $orders = Order::with('produk', 'toko')->where('user_id', $user_id)->where('no_pesanan', $no_pesanan)->get();
        if ($orders->isNotEmpty()) {
            $ordersByStore = $orders->groupBy('toko.nama');
            $totalSubtotal = $this->biayaLayanan;
            foreach ($ordersByStore as $storeName => $orders) {
                $totalSubtotal += $orders->first()->sub_total + $this->biayaPengiriman;
            }
            $totalBelanja = $totalSubtotal;
            // dd($totalBelanja);
            $biayaLayanan = $this->biayaLayanan;
            $biayaPenanganan = $this->biayaPenanganan;
            $diskonPengiriman = $this->diskonPengiriman;
            $biayaPengiriman = $this->biayaPengiriman;
            $biayaProteksi = $this->biayaProteksi;
        } else {
            return view('404');
        }
        $expiresAt = $orders->first()->created_at->addMinutes(10);
        $reviews = Review::where('user_id', $user->id)->get()->keyBy('produk_id');
        // dd($reviews);
        return view('customer.cart.detail', compact('reviews', 'expiresAt', 'orders', 'ordersByStore', 'user', 'alamat', 'biayaPengiriman', 'biayaProteksi', 'biayaLayanan', 'biayaPenanganan', 'diskonPengiriman', 'totalBelanja'));
        // dd($order);
    }
    private function generateNomorOrder()
    {
        return 'ORD' . date('YmdHis') . rand(1000, 9999); // Example: ORD202205251234560123
    }
}
