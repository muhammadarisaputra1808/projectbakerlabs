<?php

namespace App\Http\Controllers;

use App\Models\toko;
use App\Models\User;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\ProdukImage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class ProdukController extends Controller
{
    public function index(){
        $kategori = Kategori::all();
        $toko = Toko::with('user')->where('user_id', '=', auth()->user()->id)->first();
        $produk = Produk::where('user_id', '=', auth()->user()->id)->with('kategori')->get();
        // Confirmation message
        $judul = 'Hapus Data Produk!';
        $teks = "Apakah Anda yakin ingin menghapus produk ini?";
        confirmDelete($judul, $teks);
        return view('seller.produk.index', compact('toko', 'produk','kategori'));
    }

    public function create(){
        $toko = toko::with('user')->where('user_id', '=', auth()->user()->id)->first();
        $kategori = Kategori::all();
        return view('seller.produk.create', compact('toko', 'kategori'));
    }

    public function store(Request $request){
        //dd($request->all());
    
        $request->validate([
            'kategori_id' => 'required|integer',
            'nama' => 'required|string|min:3|max:255',
            'harga_list' => 'required|numeric|min:0',
            'harga_netto' => 'required|numeric|min:0',
            'stok' => 'required|integer|min:0',
            'deskripsi' => 'required|min:3|max:255',
            'images' => 'required|array|min:1', 
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ],[
            'kategori_id.required' => 'Pilih kategori *',
            'nama.required' => 'Kolom wajib di isi *',
            'nama.min' => 'Minimal 3 karakter',
            'harga_list.required' => 'Kolom wajib di isi *',
            'harga_list.numeric' => 'Masukkan angka *',
            'harga_netto.required' => 'Kolom wajib di isi *',
            'harga_netto.numeric' => 'Masukkan angka *',
            'deskripsi.min' => 'Minimal 3 karakter',
            'deskripsi.required' => 'Kolom wajib di isi *',
            'stok.required' => 'Kolom wajib di isi *',
            'stok.integer' => 'Masukkan angka *',
            'stok.min' => 'Minimal jumlah = 0 *',
            'images.required' => 'Setidaknya satu gambar harus diunggah *',
            'images.*.required' => 'Harus Gambar',
            'images.*.image' => 'File harus berupa gambar *',
            'images.*.mimes' => 'Gambar harus berformat jpeg, png, jpg, gif, svg, atau webp *',
            'images.*.max' => 'Ukuran gambar maksimal adalah 2MB *',
        ]);
    
       // dd('Validation Passed');
    
        $user_id = Auth::id();
    
        $produk = Produk::create([
            'kategori_id' => $request->kategori_id,
            'nama' => $request->nama,
            'slug' => Str::slug($request->nama),
            'harga_list' => $request->harga_list,
            'harga_netto' => $request->harga_netto,
            'stok' => $request->stok,
            'deskripsi' => $request->deskripsi,
            'user_id' => $user_id,
        ]);
    
        //dd($produk);
    
        $imageData = [];
        
        if ($files = $request->file('images')) {
            //dd('Image Uploading');
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time() . rand(1, 100) . '.' . $extension;
                $path = 'uploads/toko/';
                $file->move($path, $filename);
                $imageData[] = [
                    'produk_id' => $produk->id,
                    'images' => $path . $filename
                ];
            }
            //dd($imageData);
            ProdukImage::insert($imageData);
        }
    
        Alert::success('Hore!', 'Produk Berhasil Dibuat');
        return redirect('/toko/produk');
    }
    


    public function edit($produk_id){
        $toko = toko::with('user')->where('user_id', '=', auth()->user()->id)->first();
        $produk = Produk::findOrFail($produk_id);
        $kategori = Kategori::all();
        $title = 'Hapus Gambar!';
        $text = "Apakah Anda yakin ingin menghapus gambar ini?";
        confirmDelete($title, $text);
        return view('seller.produk.edit', compact('toko', 'produk', 'kategori'));
    }

    public function update(Request $request, $produk_id)
    {
        $request->validate([
            'kategori_id' => 'required|integer',
            'nama' => 'required|string|min:3|max:255',
            'harga_list' => 'required|numeric|min:0',
            'harga_netto' => 'required|numeric|min:0',
            'stok' => 'required|integer|min:0',
            'deskripsi' => 'required|min:3|max:255',
            'images.*' => 'nullable|image|mimes:png,jpg,jpeg,webp',
        ], [
            'kategori_id.required' => 'Pilih kategori *',
            'nama.required' => 'Kolom wajib di isi *',
            'nama.min' => 'Minimal 3 karakter',
            'harga_list.required' => 'Kolom wajib di isi *',
            'harga_list.numeric' => 'Masukkan angka *',
            'harga_netto.required' => 'Kolom wajib di isi *',
            'harga_netto.numeric' => 'Masukkan angka *',
            'deskripsi.min' => 'Minimal 3 karakter',
            'deskripsi.required' => 'Kolom wajib di isi *',
            'stok.required' => 'Kolom wajib di isi *',
            'stok.integer' => 'Masukkan angka *',
            'stok.min' => 'Minimal jumlah = 0 *',
            'images.*.image' => 'Harus Gambar'
        ]);

        $produk = Produk::findOrFail($produk_id)->update([
            'kategori_id' => $request->kategori_id,
            'nama' => $request->nama,
            'slug' => Str::slug($request->nama),
            'harga_list' => $request->harga_list,
            'harga_netto' => $request->harga_netto,
            'stok' => $request->stok,
            'deskripsi' => $request->deskripsi
        ]);
        $imageData = [];

        if ($files = $request->file('images')) {

            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time() . rand(1, 100) . '.' . $extension;
                $path = 'uploads/toko/';
                $file->move($path, $filename);
                $imageData[] = [
                    'produk_id' => $produk_id,
                    'images' => $path . $filename
                ];
            }
            ProdukImage::insert($imageData);
        } 
        Alert::success('Hore!', 'Produk Berhasil DiUbah');
        return back();
    }
    public function deleteImage($id)
    {
        // Temukan gambar berdasarkan ID
        $image = ProdukImage::findOrFail($id);
        
        // Hapus file dari sistem file;
        if (File::exists($image->images)) {
            File::delete($image->images);
        }

        // Hapus data gambar dari database
        $image->delete();
        alert()->success('Hore!','Gambar Berhasil Dihapus');
        // Kembalikan respon JSON
        return back();
    }

    public function destroy($produk_id){
        // Temukan gambar berdasarkan ID
        $produk = Produk::findOrFail($produk_id);
        $images = ProdukImage::where('produk_id',$produk_id)->get();
        
        // dd($images);
        foreach($images as $image){
            // Hapus file dari sistem file;
            if (File::exists($image->images)) {
                File::delete($image->images);
            }
        }
        $produk->delete();
        alert()->success('Hore!','Data Produk Berhasil Dihapus');
        return back();
    }
}
