<?php

namespace App\Http\Controllers;

use App\Models\toko;
use App\Models\Saldo;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class AdminController extends Controller
{
    public function index(){
        $toko = toko::all();
        $requestSaldo = Saldo::where('type', 'penarikan')->get();

        return view('admin.index', compact('toko', 'requestSaldo'));
    }
    public function showToko(){
        $toko = toko::all();
        return view('admin.toko', compact('toko'));
    }
    public function showRequest(){
        $toko = toko::all();
        $requestSaldo = Saldo::where('type', 'penarikan')->with('toko')->get();
        // dd($requestSaldo);
        return view('admin.saldo', compact('requestSaldo', 'toko'));
    }
    public function acceptRequest(Saldo $saldo){
        // dd($saldo);
        $toko = Toko::findOrFail($saldo->toko_id);
        $jumlah = $saldo->jumlah;
        $keterangan = 'Terima Penarikan Saldo';
        if ($toko->kurangiSaldo($jumlah, $keterangan)) {
            Alert::success('Hore!', 'Saldo Berhasil Dikurangi !');
            $saldo->keterangan = 'Request Penarikan Saldo Diterima';
            $saldo->save();
            return back();
        } else {
            Alert::warning('Warning!', 'Saldo Tidak Mencukupi !');
            return back();
        }
    }
}
