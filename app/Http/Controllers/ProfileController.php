<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Order;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{

    public function getJmlDikemas(){
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $orders = Order::with('produk')->with('toko')->where('user_id', $user->id)->get();
        $jmlDikemas = Order::where('user_id', $user->id)
                            ->where('status', 'Dikemas')
                            ->distinct('no_pesanan')
                            ->count('no_pesanan');
        return $jmlDikemas;
    }

    public function getJmlDikirim(){
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $orders = Order::with('produk')->with('toko')->where('user_id', $user->id)->get();
        $jmlDikirim = Order::where('user_id', $user->id)
                            ->where('status', 'Dikirim')
                            ->distinct('no_pesanan')
                            ->count('no_pesanan');
        return $jmlDikirim;
    }
    public function getJmlDiterima(){
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $orders = Order::with('produk')->with('toko')->where('user_id', $user->id)->get();
        $jmlDiterima = Order::where('user_id', $user->id)
                            ->where('status', 'Diterima')
                            ->distinct('no_pesanan')
                            ->count('no_pesanan');
        return $jmlDiterima;
    }
    public function getPesanan(){
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $orders = Order::with('produk')->with('toko')->where('user_id', $user->id)->get();
        $pesanan = $orders->groupBy('no_pesanan')->map(function ($group) {
            return $group->first();
        });
        return $pesanan;
    }
    public function index() {
        $kecamatan = [
            'Alang-alang Lebar', 'Bukit Kecil', 'Gandus', 'Ilir Barat I', 'Ilir Barat II', 
            'Ilir Timur I', 'Ilir Timur II', 'Ilir Timur III', 'Jakabaring', 'Kalidoni',
            'Kemuning', 'Kertapati', 'Plaju', 'Sako', 'Seberang Ulu I', 'Seberang Ulu II', 
            'Sematang Borang', 'Sukarami'
        ];
    
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $jmlDikemas = $this->getJmlDikemas();
        $jmlDikirim = $this->getJmlDikirim();
        $jmlDiterima = $this->getJmlDiterima();
        $pesanan = $this->getPesanan();

        return view('customer.profile.index', compact('jmlDikemas','jmlDikirim','jmlDiterima', 'kecamatan', 'user', 'pesanan'));
    }

    public function showAlamat(){
        
        $kecamatan = ['Alang-alang Lebar', 'Bukit Kecil', '	Gandus', 'Ilir Barat I', 'Ilir Barat II', 'Ilir Timur I', 'Ilir Timur II', 'Ilir Timur III', 'Jakabaring', 'Kalidoni',
            'Kemuning', 'Kertapati', 'Plaju', 'Sako', 'Seberang Ulu I', 'Seberang Ulu II', 'Sematang Borang', 'Sukarami'
        ];
        $id_user = Auth::user()->id;
        $profile = Profile::where('user_id', '=', $id_user)->first();
        $user = User::with('profile')->where('id', '=', Auth::user()->id)->first();
        $jmlDikemas = $this->getJmlDikemas();
        $jmlDikirim = $this->getJmlDikirim();
        $jmlDiterima = $this->getJmlDiterima();
        $pesanan = $this->getPesanan();
        // dd($profile);
        if(!$profile){
            return view('customer.profile.alamat', compact('jmlDikemas','jmlDikirim','jmlDiterima', 'pesanan', 'kecamatan', 'user'));
        }else{
            return view('customer.profile.index', compact('kecamatan', 'profile'));
        }
    }
    
    public function storeAlamat(Request $request){
        $request->validate([
            'nama' => 'required|string|min:3|max:255|unique:tokos,nama',
            'no_telepon' => 'required|string|min:10|max:15',
            'kecamatan' => 'required|string',
            'alamat' => 'required|string|min:10|max:255',
            'kode_pos' => 'required|string|min:3|max:5',
            'image' => 'nullable|image|mimes:png,jpg,jpeg,webp',
        ],[
            'nama.required' => 'Kolom wajib di isi *',
            'nama.min' => 'Minimal 3 karakter',
            'nama.unique' => 'Nama Sudah Digunakan',
            'no_telepon.required' => 'Kolom wajib di isi *',
            'no_telepon.min' => 'Minimal 10 karakter',
            'no_telepon.max' => 'Maksimal 15 karakter',
            'kecamatan.required' => 'Kolom wajib di isi *',
            'kode_pos.required' => 'Kolom wajib di isi *',
            'kode_pos.min' => 'Minimal 3 Karakter *',
            'kode_pos.max' => 'Maksimal 5 Karakter *',
            'alamat.required' => 'Kolom wajib di isi *',
            'alamat.min' => 'Minimal 10 Karakter *',
        ]);
        $user_id = $request->user()->id;
        Profile::updateOrCreate([
            'nama' => $request->nama,
            'user_id' => $user_id,
            'no_telepon' => $request->no_telepon,
            'kecamatan' => $request->kecamatan.', Palembang, Sumatera Selatan.',
            'alamat' => $request->alamat,
            'kode_pos' => $request->kode_pos,
        ]);
        Alert::success('Hore!', 'Berhasil Menambahkan Alamat');
        return redirect('akun');
    }

    public function editAlamat($id_profile){
        $kecamatan = ['Alang-alang Lebar', 'Bukit Kecil', '	Gandus', 'Ilir Barat I', 'Ilir Barat II', 'Ilir Timur I', 'Ilir Timur II', 'Ilir Timur III', 'Jakabaring', 'Kalidoni',
            'Kemuning', 'Kertapati', 'Plaju', 'Sako', 'Seberang Ulu I', 'Seberang Ulu II', 'Sematang Borang', 'Sukarami'
        ];
        $id_user = Auth::user()->id;
        $profile = Profile::with('user')->where('id', '=', $id_profile)->first();
        $user = User::with('profile')->where('id', Auth::user()->id)->first();
        $orders = Order::with('produk')->where('user_id', $user->id)->where('status', 'Diterima')->get();
        $jmlDikemas = $this->getJmlDikemas();
        $jmlDikirim = $this->getJmlDikirim();
        $jmlDiterima = $this->getJmlDiterima();
        // dd($orders);
        $totalQty = $orders ? $orders->sum('qty') : null;
        // dd($profile->user_id);
        if($profile){
            if($profile->user_id == $id_user){
                return view('customer.profile.edit', compact('jmlDikemas','jmlDikirim','jmlDiterima', 'kecamatan', 'profile', 'user', 'orders', 'totalQty'));
            }else{
                return view('404');
            }
        }else{
            return view('404');
        }
        // dd($profile);
    }

    public function updateAlamat(Request $request, $id_profile){
        // dd($request->all());
        $request->validate([
            'nama' => 'required|string|min:3|max:255|unique:tokos,nama',
            'no_telepon' => 'required|string|min:10|max:15',
            'kecamatan' => 'required|string',
            'alamat' => 'required|string|min:10|max:255',
            'kode_pos' => 'required|string|min:3|max:5',
            'image' => 'nullable|image|mimes:png,jpg,jpeg,webp',
        ],[
            'nama.required' => 'Kolom wajib di isi *',
            'nama.min' => 'Minimal 3 karakter',
            'nama.unique' => 'Nama Sudah Digunakan',
            'no_telepon.required' => 'Kolom wajib di isi *',
            'no_telepon.min' => 'Minimal 10 karakter',
            'no_telepon.max' => 'Maksimal 15 karakter',
            'kecamatan.required' => 'Kolom wajib di isi *',
            'kode_pos.required' => 'Kolom wajib di isi *',
            'kode_pos.min' => 'Minimal 3 Karakter *',
            'kode_pos.max' => 'Maksimal 5 Karakter *',
            'alamat.required' => 'Kolom wajib di isi *',
            'alamat.min' => 'Minimal 10 Karakter *',
        ]);
        $user_id = Auth()->user()->id;
        Profile::findOrFail($id_profile)->update([
            'nama' => $request->nama,
            'user_id' => $user_id,
            'no_telepon' => $request->no_telepon,
            'kecamatan' => $request->kecamatan.', Palembang, Sumatera Selatan.',
            'alamat' => $request->alamat,
            'kode_pos' => $request->kode_pos,
        ]);
        Alert::success('Hore!', 'Berhasil Memperbarui Alamat');
        return redirect('akun');
    }

    public function updatePhoto(Request $request, $profile_id){
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048'
        ],[
            'image.required' => 'Pilih Gambar Terlebih dahulu **',
        ]);
        if($request->has('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $path = 'uploads/toko/';
            $file->move($path, $filename);
        }else{
            $path = 'img/';
            $filename = 'default.jpg';
        }
        Profile::findOrFail($profile_id)->update([
            'image' => $path.$filename
        ]);
        Alert::success('Hore!', 'Berhasil Memperbarui Gambar');
        return back();
    }
}
