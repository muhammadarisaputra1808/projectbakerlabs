<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderReceived extends Notification
{
    use Queueable;

    protected $orders;
    protected $totalBelanja;
    protected $tokoName;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orders, $totalBelanja, $tokoName)
    {
        $this->orders = $orders;
        $this->totalBelanja = $totalBelanja;
        $this->tokoName = $tokoName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Notifikasi Pesanan Masuk')
            ->greeting('Hello ' . $this->tokoName . ',')
            ->line('Kamu telah menerima pesanan baru.')
            ->line('Detali Pesanan :')
            ->line('Total Belanja: ' . $this->totalBelanja)
            ->line('Items :')
            ->with($this->orders->map(function($order) {
                return $order->produk->nama . ' - ' . $order->qty . ' x ' . $order->produk->harga_netto;
            })->toArray())
            ->action('Lihat Pesanan', url('/orders/' . $this->orders->first()->no_pesanan))
            ->line('Terima kasih telah mempercayai BakerLabs untuk kebutuhan Anda.')
            ->salutation('Salam hangat, Tim BakerLabs');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'orders' => $this->orders,
            'total_belanja' => $this->totalBelanja,
            'toko_name' => $this->tokoName,
        ];
    }
}
