<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderProcessed extends Notification
{
    use Queueable;

    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pengiriman Pesanan')
                    ->line('Pesanan Masuk Dengan Nomor Pesanan : ' . $this->order->no_pesanan . ' Telah Diproses Dan Siap Untuk Dikirim Harap Segera Lakukan Pengiriman. ')
                    ->line('Alamat Penerima : ')
                    ->line($this->order->user->profile->alamat . ', ' . $this->order->user->profile->getKecamatanFullAttributes() . ', '. $this->order->user->profile->kode_pos)
                    ->line('Nomor Telepon/Whatsapp Penerima : ')
                    ->line($this->order->user->profile->no_telepon);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
