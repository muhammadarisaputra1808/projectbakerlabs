<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderPlaced extends Notification
{
    use Queueable;

    private $order;
    private $user;
    private $subtotal;

    public function __construct($order, $subtotal, $user)
    {
        $this->order = $order;
        $this->subtotal = $subtotal;
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Konfirmasi Pemesanan')
                    ->greeting('Halo ' . $this->user . ',')
                    ->line('Terima kasih telah berbelanja di BakerLabs! Kami dengan senang hati mengonfirmasi pesanan Anda dengan rincian sebagai berikut:')
                    ->line('Detail Pesanan')
                    ->line('Order ID: ' . $this->order->no_pesanan)
                    ->line('Total Belanja: Rp. ' . number_format($this->subtotal, 0, ',', '.') )
                    ->line('Pesanan Anda sedang diproses dan kami akan mengirimkan pembaruan lebih lanjut saat pesanan Anda telah dikirim. Jika Anda memiliki pertanyaan atau membutuhkan bantuan lebih lanjut, jangan ragu untuk menghubungi kami.')
                    ->action('Lihat Pesanan', url('/pesanan/'.$this->order->no_pesanan))
                    ->line('Terima kasih telah mempercayai BakerLabs untuk kebutuhan Anda.')
                    ->salutation('Salam hangat, Tim BakerLabs');
    }

    public function toArray($notifiable)
    {
        return [
            'order_id' => $this->order->no_pesanan,
            'order_total' => $this->subtotal,
        ];
    }
}
