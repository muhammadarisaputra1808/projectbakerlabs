<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class toko extends Model
{
    use HasFactory;

    protected $table = 'tokos';

    protected $fillable = [
        'nama',
        'user_id',
        'no_telepon',
        'kecamatan',
        'alamat',
        'kode_pos',
        'image',
        'nomor_rekening',
        'nama_pemilik',
        'bank'
    ];
    public function saldo()
    {
        return $this->hasMany(Saldo::class);
    }
    public function tambahSaldo($jumlah, $keterangan = null, $tanggal = null )
    {
        $saldo = new Saldo([
            'jumlah' => $jumlah,
            'type' => 'transaksi',
            'keterangan' => $keterangan,
            'tanggal_penarikan' => $tanggal ?: now(),
        ]);
        $this->saldo()->save($saldo);
    }
    public function kurangiSaldo($jumlah, $keterangan = null)
    {
        if ($this->getSaldoTotal() >= $jumlah) {
            $saldo = new Saldo([
                'jumlah' => -$jumlah,
                'type' => 'transaksi',
                'keterangan' => $keterangan,
            ]);
            $this->saldo()->save($saldo);
            return true;
        }
        return false;
    }
    public function catatPenarikan($jumlah, $keterangan = null, $tanggal = null)
    {
        if ($this->getSaldoTotal() >= $jumlah) {
            $saldo = new Saldo([
                'jumlah' => $jumlah,
                'type' => 'penarikan',
                'keterangan' => $keterangan,
                'tanggal_penarikan' => $tanggal ?: now(),
            ]);
            $this->saldo()->save($saldo);
            return true;
        }
        return false;
    }
    // Metode untuk mendapatkan total saldo
    public function getSaldoTotal()
    {
        // $kredit = $this->saldo()->where('jenis', 'kredit')->sum('jumlah');
        // $debit = $this->saldo()->where('jenis', 'debit')->sum('jumlah');
        return $this->saldo()->where('type', 'transaksi')->sum('jumlah');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function produks(){
        return $this->hasMany(Produk::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function getKecamatanAttribute($value)
    {
        // Memisahkan nilai kecamatan berdasarkan koma dan mengambil elemen pertama
        $parts = explode(',', $value);
        return isset($parts[0]) ? $parts[0] : $value;
    }
    public function getKecamatanFullAttributes(){
        return $this->attributes['kecamatan'];
    }
    public function setNoTeleponAttribute($value)
    {
        $this->attributes['no_telepon'] = $this->formatNomorTelepon($value);
    }
    public function getNoTeleponAttribute($value)
    {
        return $this->formatNomorTeleponLokal($value);
    }
    // Accessor untuk mendapatkan nomor telepon dengan kode negara untuk URL
    public function getNoTeleponForUrlAttribute()
    {
        return $this->formatNomorTelepon($this->attributes['no_telepon']);
    }
    private function formatNomorTeleponLokal($no_telepon)
    {
        // Jika nomor dimulai dengan '62', ubah menjadi '0'
        if (substr($no_telepon, 0, 2) === '62') {
            $no_telepon = '0' . substr($no_telepon, 2);
        }
        
        return $no_telepon;
    }

    // Fungsi untuk memformat nomor telepon
    private function formatNomorTelepon($no_telepon)
    {
        // Hapus spasi, tanda kurung, dan strip dari nomor telepon
        $no_telepon = preg_replace('/[^0-9]/', '', $no_telepon);
        
        // Jika nomor dimulai dengan '0', ganti dengan kode negara Indonesia '62'
        if (substr($no_telepon, 0, 1) === '0') {
            $no_telepon = '62' . substr($no_telepon, 1);
        }
        
        // Jika nomor sudah dimulai dengan '62', tidak perlu diubah
        if (substr($no_telepon, 0, 2) !== '62') {
            $no_telepon = '62' . $no_telepon;
        }
        
        return $no_telepon;
    }
}
