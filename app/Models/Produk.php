<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produks';

    protected $fillable = [
        'kategori_id',
        'user_id',
        'nama',
        'slug',
        'harga_list',
        'harga_netto',
        'stok',
        'deskripsi',
    ];

    public function kategori(){
        return $this->belongsTo(Kategori::class, 'kategori_id', 'id');
    }
    public function images(){
        return $this->hasMany(ProdukImage::class);
    }
    public function toko()
    {
        return $this->belongsTo(Toko::class, 'user_id', 'user_id');
    }
    public function cart(){
        return $this->hasMany(Cart::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function getAcceptedOrdersCountAttribute()
    {
        return $this->orders()->where('status', 'Diterima')->count();
    }
    public function reviews() {
        return $this->hasMany(Review::class);
    }
}
