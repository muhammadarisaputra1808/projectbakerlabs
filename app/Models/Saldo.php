<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Saldo extends Model
{
    use HasFactory;
    protected $table = 'saldos';

    protected $fillable = [
        'toko_id',
        'jumlah',
        'type',      
        'keterangan',
        'tanggal_penarikan',  // Tambahkan atribut tanggal_penarikan di sini
    ];

    public function toko()
    {
        return $this->belongsTo(Toko::class);
    }
}
