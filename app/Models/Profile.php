<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profiles';
    protected $fillable = [
        'nama',
        'user_id',
        'no_telepon',
        'kecamatan',
        'alamat',
        'kode_pos',
        'image',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getKecamatanAttribute($value)
    {
        // Memisahkan nilai kecamatan berdasarkan koma dan mengambil elemen pertama
        $parts = explode(',', $value);
        return isset($parts[0]) ? $parts[0] : $value;
    }
    public function getKecamatanFullAttributes(){
        return $this->attributes['kecamatan'];
    }
    
}
