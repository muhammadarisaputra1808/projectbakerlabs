<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'produk_id',
        'toko_id',
        'qty',
        'total',
        'sub_total',
        'no_pesanan',
        'tgl_pesanan',
        'snap_token',
        'status',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id', 'id');
    }
    public function toko(){
        return $this->belongsTo(toko::class, 'toko_id', 'id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
