<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CustomerController;

Auth::routes();

Route::get('/',[CustomerController::class,'index']);
Route::get('/tentang-kami',[CustomerController::class,'tentangkami']);

Route::middleware(['auth'])->group(function(){
    //CART
    Route::get('/cart',[CartController::class,'index']);
    Route::get('/checkout', [CartController::class, 'checkout'])->name('checkout');
    Route::get('/pesanan', [CartController::class, 'pesanan'])->name('pesanan');
    Route::get('/checkout/success/{no_pesanan}', [CartController::class, 'success'])->name('checkout-success');
    Route::get('/checkout/cancel/{no_pesanan}', [CartController::class, 'cancelOrder'])->name('cancel-order');
    Route::get('/pesanan/{details}', [CartController::class, 'detailPesanan'])->name('pesanan.show.details');
    Route::post('/add-to-cart', [CartController::class, 'addToCart'])->name('add-to-cart');
    Route::get('/remove-from-cart/{id}', [CartController::class, 'removeFromCart'])->name('remove-from-cart');
    Route::post('/update-cart/{id}', [CartController::class, 'updateCart'])->name('update-cart');
    // Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout');
    Route::delete('/orders/{no_pesanan}', [CartController::class, 'deleteOrder']);
    Route::get('/pesanan/{no_pesanan}/proses', [CartController::class, 'finishOrder'])->name('finish.order');
    Route::post('/pesanan/{produk}/review', [ReviewController::class, 'store'])->name('produk.review.store');
    Route::put('/pesanan/{review}/review', [ReviewController::class, 'update'])->name('produk.review.update');
    Route::get('daftartoko', [SellerController::class, 'createtoko']);
    Route::post('daftartoko', [SellerController::class, 'storetoko']);
    Route::get('/admin',[AdminController::class,'index']); 
});

Route::prefix('akun')->middleware(['auth'])->group(function(){
    Route::get('/', [ProfileController::class, 'index'])->name('profile.view');
    Route::put('/update-photo/{profile_id}', [ProfileController::class, 'updatePhoto'])->name('profile.photo.update');
    Route::get('/alamat', [ProfileController::class, 'showAlamat'])->name('profile.alamat.show');
    Route::post('/alamat', [ProfileController::class, 'storeAlamat'])->name('profile.alamat.save');
    Route::get('/alamat/ubah/{id}', [ProfileController::class, 'editAlamat'])->name('profile.alamat.edit');
    Route::put('/alamat/ubah/{id}', [ProfileController::class, 'updateAlamat'])->name('profile.alamat.update');
});

Route::prefix('menu')->group(function(){
    Route::get('/', [CustomerController::class, 'showMenu'])->name('produks.show');
    Route::get('/search', [CustomerController::class, 'search'])->name('produks.search');
    Route::get('/{produk_slug}/details', [CustomerController::class, 'showProdukDetail']);
    Route::get('/{kategorislug}', [CustomerController::class, 'showKategori']);
    Route::get('/{kategorislug}/q', [CustomerController::class, 'sortBy'])->name('produks.sortby');
    Route::get('/{kategorislug}/{produk_slug}', [CustomerController::class, 'showKategoriProduk']);
});

Route::prefix('toko')->middleware(['auth', 'seller'])->group(function(){
    Route::get('/', [SellerController::class, 'index'])->name('dashboard');
    Route::get('/pengaturan', [SellerController::class, 'editToko'])->name('edit-toko');
    Route::put('/pengaturan/{toko_id}', [SellerController::class, 'updateToko'])->name('update-toko');
    Route::put('/update-photo/{toko_id}', [SellerController::class, 'updatePhoto'])->name('profile.photo.toko.update');
    
    Route::get('produk', [ProdukController::class, 'index']);
    Route::get('produk/tambah', [ProdukController::class, 'create']);
    Route::post('produk/tambah', [ProdukController::class, 'store']);
    Route::get('produk/edit/{produk_id}', [ProdukController::class, 'edit']);
    Route::put('produk/edit/{produk_id}', [ProdukController::class, 'update']);
    Route::get('produk/hapus/{produk_id}', [ProdukController::class, 'destroy'])->name('produk.destroy');
    Route::delete('/delete-image/{id}', [ProdukController::class, 'deleteImage'])->name('delete.image');
    
    Route::get('pesanan', [PesananController::class, 'index'])->name('pesanan');
    Route::get('pesanan/{no_pesanan}', [PesananController::class, 'detailPesanan'])->name('detail-pesanan');
    Route::get('pesanan/proses/{no_pesanan}', [PesananController::class, 'prosesPesanan'])->name('proses-pesanan');
    
    Route::get('saldo', [SellerController::class, 'showSaldo'])->name('show.saldo');
    Route::put('/saldo/{toko_id}', [SellerController::class, 'updateNomorRekening'])->name('update.no.rekening');
    Route::post('/saldo/{toko_id}', [SellerController::class, 'catatPenarikan'])->name('simpan.nominal');
});

Route::prefix('admin')->middleware(['auth', 'admin'])->group(function(){
    Route::get('/', [AdminController::class, 'index'])->name('dashboard.admin');
    Route::get('toko', [AdminController::class, 'showToko'])->name('show.toko');
    Route::get('saldo', [AdminController::class, 'showRequest'])->name('show.saldo');
    Route::get('saldo/terima/{saldo}', [AdminController::class, 'acceptRequest'])->name('accept.request');
});